Feature: The user is navigate to Jegytipusok page and check the url

  Scenario: The user is make a new program location
    Given I will navigate to "appUrl" page
    When I authenticate with "username" and "password"
    And I navigate to Jegytipusok page
    And I check the url
    Then I click on logout button