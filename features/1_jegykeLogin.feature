Feature: The user is authenticated into Jegyke application via KAÜ

  Scenario: User give correct credentials
    Given I will navigate to "appUrl" page
    When I click  on login button
    And I click on ügyfélkapu button
    And I fill the "Username" and "Password" fields
    Then I click on logout button
