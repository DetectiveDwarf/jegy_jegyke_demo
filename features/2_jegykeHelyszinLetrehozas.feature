Feature: The user is create new program location for future usage

  Scenario: The user is make a new program location
    Given I will navigate to "appUrl" page
    When I authenticate with "username" and "password"
    And I navigate to Programhelyszínek page
    And I click on Új programhelyszín hozzáadása button
    And I fill the mandatory fields with valid data on Programhelyszín létrehozása page
    And I click on mentés button
    And I click on bezárás button
    And I create a program helyszín with missing mandatory data
    And I click on mentés button
    And I click on modosítások elvetése button
    And I click on bezárás button
    Then I click on logout button
