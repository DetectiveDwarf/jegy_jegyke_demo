Feature: The user is navigate to Jegytipusok page and create jegytípus

  Scenario: The user is make a new program location
    Given I will navigate to "appUrl" page
    When I authenticate with "username" and "password"
    And I navigate to Jegytipusok page
    And I click on Új jegytípus hozzáadása button
    And I make a new jegytípus and check the newly created jegytípus in the list page
    And I make a new family jegytipus with discount and I check the newly created family jegytípus in the list page
    And I create a new jegytipus than edit it
    And I click on modosítások elvetése button
    And I create a jegytipus than inactivate it and I check the inactivated jegytipus among the jegytipusok with active state
    And I check the inactivated jegytipus among the jegytipusok with inactive state
    Then I click on logout button