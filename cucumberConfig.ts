import {Config} from 'protractor';

import * as reporter from 'cucumber-html-reporter' ;

// @ts-ignore
export let config: Config = {

// The address of a running selenium server.

    directConnect: true,
    framework: 'custom',
    frameworkPath: require.resolve('protractor-cucumber-framework'),

// Capabilities to be passed to the webdriver instance.

    capabilities: {
        browserName: 'chrome',
        chromeOptions: {
            args: [
                '--no-sandbox',
                //'--headless',
                '--disable-gpu',
                '--window-size=1280,1024',
                '--ignore-certificate-errors',
                '--ignore-ssl-errors',
                '--disable-web-security',
            ],
        },
    },
// Spec patterns are relative to the configuration file location passed

// to protractor (in this example conf.js).

// They may include glob patterns.

    specs: ['../features/*'], //use .js only as this file will convert to .js and will be used to run
    cucumberOpts: {
// require step definitions
// tags: "@cDivision",
        format: 'json:./cucumberreport.json',
        require: [
            './stepDefinitions/*.js' // accepts a glob
        ]
    }, //end of cucumberOpts
    onComplete: () =>{
        var options = {
            theme: 'bootstrap',
            jsonFile: './cucumberreport.json',
            output: './cucumberreport.html',
            reportSuiteAsScenarios: true,
            scenarioTimestamp: true,
            launchReport: true,
            metadata: {
                "App Version":"0.3.2",
                "Test Environment": "STAGING",
                "Browser": "Chrome 54.0.2840.98",
                "Platform": "Windows 10",
                "Parallel": "Scenarios",
                "Executed": "Remote"
            }
        };
        reporter.generate(options);
    }// end of on complete
};