import {Given, Then, When} from "cucumber";
import {browser} from "protractor";
import { App } from '../pageObjects/app.po';
import chai from "chai";

var expect = chai.expect;
browser.waitForAngularEnabled(false);
let app = new App();

Given('I will navigate to {string} page', async (string)=> {
    // Write code here that turns the phrase above into concrete actions
    await browser.get("https://jegyke-qa.sonrisa.hu/login");
});

When('I click  on login button', async ()=> {
    // Write code here that turns the phrase above into concrete actions
    await app.kauLoginPage.kauButton.click();
});

When('I click on ügyfélkapu button', async ()=> {
    // Write code here that turns the phrase above into concrete actions
    await app.kauLoginPage.ugyfelkapuButton.click();
});

When('I fill the {string} and {string} fields', async (string, string2)=> {
    // Write code here that turns the phrase above into concrete actions
    await app.kauLoginPage.kauLogin();
});

Then('I click on logout button', async ()=> {
    // Write code here that turns the phrase above into concrete actions
    await app.menu.logout();
});
