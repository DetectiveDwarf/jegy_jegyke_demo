import {When} from "cucumber";
import {App} from "../pageObjects/app.po";
import {Form} from "../pageObjects/form.po";
import {browser} from "protractor";

let app = new App();
When('I click on Új jegytípus hozzáadása button', async ()=> {
    // Write code here that turns the phrase above into concrete actions
    await app.jegytipusPage.ujJegytipusButton.click();
});

When('I make a new jegytípus and check the newly created jegytípus in the list page', async ()=> {
    // Write code here that turns the phrase above into concrete actions
    let jegytipusName = 'jegytipus ' + Date.now();
    await app.menu.goToJegytipusok();
    await app.jegytipusPage.ujJegytipusButton.click();
    await app.jegytipusDetailPage.megnevezesField.sendKeys(jegytipusName);
    await Form.selectDropdownItem(app.jegytipusDetailPage.korcsoportDropdown, 'Felnőtt (25-63)');
    await Form.selectDropdownItem(app.jegytipusDetailPage.jegytipusKategoriaDropdown, 'Egyéni');
    await app.jegytipusDetailPage.mentesButton.click();
    await app.jegytipusDetailPage.bezarasButton.click();
    await app.jegytipusPage.searchField.sendKeys(jegytipusName);
});

When('I make a new family jegytipus with discount and I check the newly created family jegytípus in the list page', async ()=> {
    // Write code here that turns the phrase above into concrete actions
    let jegytipusName = 'jegytipus ' + Date.now();
    await app.menu.goToJegytipusok();
    await app.jegytipusPage.ujJegytipusButton.click();
    await app.jegytipusDetailPage.megnevezesField.sendKeys(jegytipusName);
    await Form.selectDropdownItem(app.jegytipusDetailPage.jegyArTipusaDropdown, 'Kedvezményes');
    await app.jegytipusDetailPage.ujkedvezmenyButton.click();
    await Form.selectDropdownItem(app.jegytipusDetailPage.kedvezmenyDropdown, 'Családi kedvezmény');
    await app.jegytipusDetailPage.hozzaadasButton.click();
    await Form.selectDropdownItem(app.jegytipusDetailPage.korcsoportDropdown, 'Vegyes (0-99)');
    await Form.selectDropdownItem(app.jegytipusDetailPage.jegytipusKategoriaDropdown, 'Családi');
    await Form.selectRadioButton(app.jegytipusDetailPage.szemelyekSzamaRadioButton, 'Csoportos');
    await app.jegytipusDetailPage.szemelyekSzamaField.sendKeys(5);
    await app.jegytipusDetailPage.mentesButton.click();
    await app.jegytipusDetailPage.bezarasButton.click();
    await app.jegytipusPage.searchField.sendKeys(jegytipusName);
});

When('I create a new jegytipus than edit it', async ()=> {
    // Write code here that turns the phrase above into concrete actions
    let jegytipusName = 'jegytipus ' + Date.now();
    await app.menu.goToJegytipusok();
    await app.jegytipusPage.ujJegytipusButton.click();
    await app.jegytipusDetailPage.megnevezesField.sendKeys(jegytipusName);
    await app.jegytipusDetailPage.rovidKodField.sendKeys('AUTST');
    await app.jegytipusDetailPage.rovidLeirasField.sendKeys('test leírás automata tesztből');
    await Form.selectDropdownItem(app.jegytipusDetailPage.jegyArTipusaDropdown, 'Kedvezményes');
    await app.jegytipusDetailPage.ujkedvezmenyButton.click();
    await Form.selectDropdownItem(app.jegytipusDetailPage.kedvezmenyDropdown, 'Egyéb kártyakedvezmények');
    await app.jegytipusDetailPage.hozzaadasButton.click();
    await Form.selectDropdownItem(app.jegytipusDetailPage.korcsoportDropdown, 'Idős: (70-');
    await Form.selectDropdownItem(app.jegytipusDetailPage.jegytipusKategoriaDropdown, 'Egyéni');
    await app.jegytipusDetailPage.mentesButton.click();
    await Form.selectDropdownItem(app.jegytipusDetailPage.jegyArTipusaDropdown, 'Teljes árú');
});

When('I create a jegytipus than inactivate it and I check the inactivated jegytipus among the jegytipusok with active state', async ()=> {
    // Write code here that turns the phrase above into concrete actions
    let jegytipusName = 'jegytipus ' + Date.now();
    await app.menu.goToJegytipusok();
    await app.jegytipusPage.ujJegytipusButton.click();
    await app.jegytipusDetailPage.megnevezesField.sendKeys(jegytipusName);
    await Form.selectDropdownItem(app.jegytipusDetailPage.korcsoportDropdown, 'Felnőtt (25-63)');
    await Form.selectDropdownItem(app.jegytipusDetailPage.jegytipusKategoriaDropdown, 'Egyéni');
    await app.jegytipusDetailPage.mentesButton.click();
    await app.jegytipusDetailPage.inactivateButton.click();
    await app.jegytipusDetailPage.confirmButton.click();
    await app.jegytipusPage.searchField.sendKeys(jegytipusName);
});

When('I check the inactivated jegytipus among the jegytipusok with inactive state', async ()=> {
    // Write code here that turns the phrase above into concrete actions
    await app.jegytipusPage.inactiveFilterCheckbox.click();
    await app.jegytipusPage.activeFilterCheckbox.click();
});