import {When} from "cucumber";
import {App} from "../pageObjects/app.po";
import {browser} from "protractor";
import chai from "chai";

let expect = chai.expect;

let app = new App();
When('I navigate to Jegytipusok page', async ()=> {
    // Write code here that turns the phrase above into concrete actions
    await app.menu.goToJegytipusok();
});

When('I check the url', async ()=> {
    // Write code here that turns the phrase above into concrete actions
    expect(await browser.getCurrentUrl()).to.Contain('/jegytipusok');
});

When('I check the filters', async ()=> {
    // Write code here that turns the phrase above into concrete actions
    return 'pending';
});
