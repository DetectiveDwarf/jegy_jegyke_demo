import {Then, When} from "cucumber";
import { browser } from 'protractor';
import { UserType } from '../models/user-type.enum';
import { App } from '../pageObjects/app.po';
import {EmailLoginPage} from "../pageObjects/login.po";


let app = new App();
let login = new EmailLoginPage();


When('I authenticate with {string} and {string}', async (string, string2)=> {
    // Write code here that turns the phrase above into concrete actions
    await login.usernameField.sendKeys('test@temp.hu');
    await login.passwordField.sendKeys('user');
    await login.loginButton.click();
});


When('I navigate to Programhelyszínek page', async ()=> {
    // Write code here that turns the phrase above into concrete actions
    await browser.sleep(6000);
    await app.menu.goToHelyszinek();
});

When('I click on Új programhelyszín hozzáadása button', async ()=> {
    // Write code here that turns the phrase above into concrete actions
    await app.helyszinPage.ujHelyszinButton.click();
});

When('I fill the mandatory fields with valid data on Programhelyszín létrehozása page', async ()=> {
    // Write code here that turns the phrase above into concrete actions
    let helyszinName = 'helyszin ' + Date.now();
    await app.helyszinDetailPage.rovidNevField.sendKeys(helyszinName);
    await app.helyszinDetailPage.hivatalosNevField.sendKeys(helyszinName);
    await app.helyszinDetailPage.iranyitoszamField.sendKeys('1111');
    await app.helyszinDetailPage.kozteruletNevDropdown.sendKeys('Budafoki');
    await app.helyszinDetailPage.hazszamField.sendKeys('5');
});

When('I click on mentés button', async ()=> {
    // Write code here that turns the phrase above into concrete actions
    await app.form.mentesButton.click();
});

When('I click on bezárás button', async ()=> {
    // Write code here that turns the phrase above into concrete actions
    await app.form.bezarasButton.click();
});

When('I create a program helyszín with missing mandatory data', async ()=> {
    // Write code here that turns the phrase above into concrete actions
    let helyszinName = 'helyszin ' + Date.now();
    await app.helyszinPage.ujHelyszinButton.click();
    await app.helyszinDetailPage.rovidNevField.sendKeys(helyszinName);
    await app.helyszinDetailPage.hivatalosNevField.sendKeys(helyszinName);
    await app.form.mentesButton.click();
});

When('I click on modosítások elvetése button', async ()=> {
    // Write code here that turns the phrase above into concrete actions
    await app.form.modositasokElveteseButton.click();
});