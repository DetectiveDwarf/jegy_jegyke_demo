import {browser} from "protractor";
import {Before, After} from "cucumber";

Before(async function () {
    // This hook will be executed before scenarios tagged with @foo
    browser.manage().window().maximize();
});

After(async function () {
    // This hook will be executed before scenarios tagged with @foo
    browser.executeScript('window.sessionStorage.clear();');
    //browser.executeScript('window.localStorage.clear();');
});

After(async function (scenario) {

    // This hook will be executed before scenarios tagged with @foo
    if(scenario.result.status === 'failed')
    {
        const screenshot = await browser.takeScreenshot();
        this.attach(screenshot,"image/png");
    }
});



