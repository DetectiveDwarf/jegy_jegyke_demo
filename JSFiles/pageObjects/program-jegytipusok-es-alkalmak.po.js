"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.JegytipusokEsAlkalmakPage = void 0;
const protractor_1 = require("protractor");
const form_po_1 = require("./form.po");
class JegytipusokEsAlkalmakPage {
    constructor() {
        this.ujAlkalomRogziteseButton = protractor_1.element(protractor_1.by.partialButtonText('Új alkalmak rögzítése'));
        this.alkalomMegnevezesField = form_po_1.Form.getInput('megnevezes');
        this.hetiIsmetlodesRadioButton = form_po_1.Form.getRadioButton('alkalmakJellege', 'Heti ismétlődés');
        this.konkretAlkalmakRadioButton = form_po_1.Form.getRadioButton('alkalmakJellege', 'Konkrét alkalmak');
        this.nincsLimitRadiobutton = form_po_1.Form.getRadioButton('ertekesitesiKorlat', 'Nincs');
        this.alkalomLimitRadiobutton = form_po_1.Form.getRadioButton('ertekesitesiKorlat', 'Alkalmakra (fő)');
        this.jegytipusLimitRadiobutton = form_po_1.Form.getRadioButton('ertekesitesiKorlat', 'Jegytípusra (db)');
        this.ervenyessegKezdeteField = form_po_1.Form.getInput('ervenyessegKezdete');
        this.ervenyessegVegeField = form_po_1.Form.getInput('ervenyessegVege');
        this.hetfoCheckbox = form_po_1.Form.getCheckboxByFormControlName('hetfo');
        this.keddCheckbox = form_po_1.Form.getCheckboxByFormControlName('kedd');
        this.szerdaCheckbox = form_po_1.Form.getCheckboxByFormControlName('szerda');
        this.csutortokCheckbox = form_po_1.Form.getCheckboxByFormControlName('csutortok');
        this.pentekCheckbox = form_po_1.Form.getCheckboxByFormControlName('pentek');
        this.szombatCheckbox = form_po_1.Form.getCheckboxByFormControlName('szombat');
        this.vasarnapCheckbox = form_po_1.Form.getCheckboxByFormControlName('vasarnap');
        this.jegytipusCheckbox = form_po_1.Form.getCheckboxByFormControlName('active');
        this.arField = form_po_1.Form.getInput('ar');
    }
}
exports.JegytipusokEsAlkalmakPage = JegytipusokEsAlkalmakPage;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicHJvZ3JhbS1qZWd5dGlwdXNvay1lcy1hbGthbG1hay5wby5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIi4uLy4uL3BhZ2VPYmplY3RzL3Byb2dyYW0tamVneXRpcHVzb2stZXMtYWxrYWxtYWsucG8udHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7O0FBQUEsMkNBQXlDO0FBQ3pDLHVDQUFpQztBQUVqQyxNQUFhLHlCQUF5QjtJQUF0QztRQUNJLDZCQUF3QixHQUFHLG9CQUFPLENBQUMsZUFBRSxDQUFDLGlCQUFpQixDQUFDLHVCQUF1QixDQUFDLENBQUMsQ0FBQztRQUNsRiwyQkFBc0IsR0FBRyxjQUFJLENBQUMsUUFBUSxDQUFDLFlBQVksQ0FBQyxDQUFDO1FBQ3JELDhCQUF5QixHQUFHLGNBQUksQ0FBQyxjQUFjLENBQUMsaUJBQWlCLEVBQUUsaUJBQWlCLENBQUMsQ0FBQztRQUN0RiwrQkFBMEIsR0FBRyxjQUFJLENBQUMsY0FBYyxDQUFDLGlCQUFpQixFQUFFLGtCQUFrQixDQUFDLENBQUM7UUFDeEYsMEJBQXFCLEdBQUcsY0FBSSxDQUFDLGNBQWMsQ0FBQyxvQkFBb0IsRUFBRSxPQUFPLENBQUMsQ0FBQztRQUMzRSw0QkFBdUIsR0FBRyxjQUFJLENBQUMsY0FBYyxDQUFDLG9CQUFvQixFQUFFLGlCQUFpQixDQUFDLENBQUM7UUFDdkYsOEJBQXlCLEdBQUcsY0FBSSxDQUFDLGNBQWMsQ0FBQyxvQkFBb0IsRUFBRSxrQkFBa0IsQ0FBQyxDQUFDO1FBQzFGLDRCQUF1QixHQUFHLGNBQUksQ0FBQyxRQUFRLENBQUMsb0JBQW9CLENBQUMsQ0FBQztRQUM5RCx5QkFBb0IsR0FBRyxjQUFJLENBQUMsUUFBUSxDQUFDLGlCQUFpQixDQUFDLENBQUM7UUFFeEQsa0JBQWEsR0FBRyxjQUFJLENBQUMsNEJBQTRCLENBQUMsT0FBTyxDQUFDLENBQUM7UUFDM0QsaUJBQVksR0FBRyxjQUFJLENBQUMsNEJBQTRCLENBQUMsTUFBTSxDQUFDLENBQUM7UUFDekQsbUJBQWMsR0FBRyxjQUFJLENBQUMsNEJBQTRCLENBQUMsUUFBUSxDQUFDLENBQUM7UUFDN0Qsc0JBQWlCLEdBQUcsY0FBSSxDQUFDLDRCQUE0QixDQUFDLFdBQVcsQ0FBQyxDQUFDO1FBQ25FLG1CQUFjLEdBQUcsY0FBSSxDQUFDLDRCQUE0QixDQUFDLFFBQVEsQ0FBQyxDQUFDO1FBQzdELG9CQUFlLEdBQUcsY0FBSSxDQUFDLDRCQUE0QixDQUFDLFNBQVMsQ0FBQyxDQUFDO1FBQy9ELHFCQUFnQixHQUFHLGNBQUksQ0FBQyw0QkFBNEIsQ0FBQyxVQUFVLENBQUMsQ0FBQztRQUVqRSxzQkFBaUIsR0FBRyxjQUFJLENBQUMsNEJBQTRCLENBQUMsUUFBUSxDQUFDLENBQUM7UUFFaEUsWUFBTyxHQUFHLGNBQUksQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLENBQUM7SUFDbEMsQ0FBQztDQUFBO0FBdEJELDhEQXNCQyJ9