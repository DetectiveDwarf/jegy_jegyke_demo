"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.HelyszinDetailPage = void 0;
const form_po_1 = require("./form.po");
class HelyszinDetailPage {
    constructor() {
        this.rovidNevField = form_po_1.Form.getInput('rovidNev');
        this.hivatalosNevField = form_po_1.Form.getInput('hosszuNev');
        this.iranyitoszamField = form_po_1.Form.getInputMask('iranyitoszam');
        this.telepulesDropdown = form_po_1.Form.getAutocomplete('telepules');
        this.kozteruletNevDropdown = form_po_1.Form.getAutocomplete('selectedAddress');
        this.kozteruletJellegDropdown = form_po_1.Form.getAutocomplete('kozteruletTipus');
        this.hazszamField = form_po_1.Form.getInput('hazszam');
        this.cimKiegeszitesField = form_po_1.Form.getInput('cimkiegeszites');
        this.megjegyzesField = form_po_1.Form.getInput('leiras');
    }
}
exports.HelyszinDetailPage = HelyszinDetailPage;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaGVseXN6aW4tZGV0YWlsLnBvLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiLi4vLi4vcGFnZU9iamVjdHMvaGVseXN6aW4tZGV0YWlsLnBvLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7OztBQUNBLHVDQUFpQztBQUVqQyxNQUFhLGtCQUFrQjtJQVczQjtRQUVJLElBQUksQ0FBQyxhQUFhLEdBQUcsY0FBSSxDQUFDLFFBQVEsQ0FBQyxVQUFVLENBQUMsQ0FBQztRQUMvQyxJQUFJLENBQUMsaUJBQWlCLEdBQUcsY0FBSSxDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUMsQ0FBQztRQUNwRCxJQUFJLENBQUMsaUJBQWlCLEdBQUcsY0FBSSxDQUFDLFlBQVksQ0FBQyxjQUFjLENBQUMsQ0FBQztRQUMzRCxJQUFJLENBQUMsaUJBQWlCLEdBQUcsY0FBSSxDQUFDLGVBQWUsQ0FBQyxXQUFXLENBQUMsQ0FBQztRQUMzRCxJQUFJLENBQUMscUJBQXFCLEdBQUcsY0FBSSxDQUFDLGVBQWUsQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDO1FBQ3JFLElBQUksQ0FBQyx3QkFBd0IsR0FBRyxjQUFJLENBQUMsZUFBZSxDQUFDLGlCQUFpQixDQUFDLENBQUM7UUFDeEUsSUFBSSxDQUFDLFlBQVksR0FBRyxjQUFJLENBQUMsUUFBUSxDQUFDLFNBQVMsQ0FBQyxDQUFDO1FBQzdDLElBQUksQ0FBQyxtQkFBbUIsR0FBRyxjQUFJLENBQUMsUUFBUSxDQUFDLGdCQUFnQixDQUFDLENBQUM7UUFDM0QsSUFBSSxDQUFDLGVBQWUsR0FBRyxjQUFJLENBQUMsUUFBUSxDQUFDLFFBQVEsQ0FBQyxDQUFDO0lBQ25ELENBQUM7Q0FlSjtBQXJDRCxnREFxQ0MifQ==