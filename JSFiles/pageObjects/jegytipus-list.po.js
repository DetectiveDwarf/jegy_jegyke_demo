"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.JegytipusListPage = void 0;
const protractor_1 = require("protractor");
const form_po_1 = require("./form.po");
class JegytipusListPage {
    constructor() {
        this.ujJegytipusButton = protractor_1.element(protractor_1.by.buttonText('Új jegytípus hozzáadása'));
        this.searchField = protractor_1.element(protractor_1.by.css('.kereses-block input'));
        this.tableRows = protractor_1.element.all(protractor_1.by.css('.divTableRow'));
        this.inactiveFilterCheckbox = form_po_1.Form.getCheckboxByName('statuszSzuro', 'Inaktív');
        this.activeFilterCheckbox = form_po_1.Form.getCheckboxByName('statuszSzuro', 'Aktív');
    }
}
exports.JegytipusListPage = JegytipusListPage;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiamVneXRpcHVzLWxpc3QucG8uanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyIuLi8uLi9wYWdlT2JqZWN0cy9qZWd5dGlwdXMtbGlzdC5wby50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7QUFBQSwyQ0FBeUM7QUFDekMsdUNBQWlDO0FBRWpDLE1BQWEsaUJBQWlCO0lBQTlCO1FBQ0ksc0JBQWlCLEdBQUcsb0JBQU8sQ0FBQyxlQUFFLENBQUMsVUFBVSxDQUFDLHlCQUF5QixDQUFDLENBQUMsQ0FBQztRQUN0RSxnQkFBVyxHQUFHLG9CQUFPLENBQUMsZUFBRSxDQUFDLEdBQUcsQ0FBQyxzQkFBc0IsQ0FBQyxDQUFDLENBQUM7UUFDdEQsY0FBUyxHQUFHLG9CQUFPLENBQUMsR0FBRyxDQUFDLGVBQUUsQ0FBQyxHQUFHLENBQUMsY0FBYyxDQUFDLENBQUMsQ0FBQztRQUNoRCwyQkFBc0IsR0FBRyxjQUFJLENBQUMsaUJBQWlCLENBQUMsY0FBYyxFQUFFLFNBQVMsQ0FBQyxDQUFDO1FBQzNFLHlCQUFvQixHQUFHLGNBQUksQ0FBQyxpQkFBaUIsQ0FBQyxjQUFjLEVBQUUsT0FBTyxDQUFDLENBQUM7SUFDM0UsQ0FBQztDQUFBO0FBTkQsOENBTUMifQ==