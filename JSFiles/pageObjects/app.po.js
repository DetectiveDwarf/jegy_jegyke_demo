"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.App = void 0;
const helyszin_detail_po_1 = require("./helyszin-detail.po");
const helyszin_list_po_1 = require("./helyszin-list.po");
const jegytipus_detail_po_1 = require("./jegytipus-detail.po");
const jegytipus_list_po_1 = require("./jegytipus-list.po");
const kau_login_po_1 = require("./kau-login.po");
const login_po_1 = require("./login.po");
const menu_po_1 = require("./menu.po");
const shell_po_1 = require("./shell.po");
const form_po_1 = require("./form.po");
const program_po_1 = require("./program.po");
const program_list_po_1 = require("./program-list.po");
class App {
    constructor() {
        this.menu = new menu_po_1.MenuPage();
        this.shell = new shell_po_1.ShellPage();
        this.form = new form_po_1.Form();
        this.helyszinDetailPage = new helyszin_detail_po_1.HelyszinDetailPage();
        this.helyszinPage = new helyszin_list_po_1.HelyszinPage();
        this.jegytipusDetailPage = new jegytipus_detail_po_1.JegytipusDetailPage();
        this.jegytipusPage = new jegytipus_list_po_1.JegytipusListPage();
        this.program = new program_po_1.ProgramPage();
        this.programListPage = new program_list_po_1.ProgramListPage();
        this.kauLoginPage = new kau_login_po_1.KauLoginPage();
        this.emailLoginPage = new login_po_1.EmailLoginPage();
    }
}
exports.App = App;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYXBwLnBvLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiLi4vLi4vcGFnZU9iamVjdHMvYXBwLnBvLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7OztBQUFBLDZEQUEwRDtBQUMxRCx5REFBa0Q7QUFDbEQsK0RBQTREO0FBQzVELDJEQUF3RDtBQUN4RCxpREFBOEM7QUFDOUMseUNBQTRDO0FBQzVDLHVDQUFxQztBQUNyQyx5Q0FBdUM7QUFDdkMsdUNBQWlDO0FBQ2pDLDZDQUEyQztBQUMzQyx1REFBb0Q7QUFFcEQsTUFBYSxHQUFHO0lBQWhCO1FBQ0ksU0FBSSxHQUFHLElBQUksa0JBQVEsRUFBRSxDQUFDO1FBRXRCLFVBQUssR0FBRyxJQUFJLG9CQUFTLEVBQUUsQ0FBQztRQUV4QixTQUFJLEdBQUcsSUFBSSxjQUFJLEVBQUUsQ0FBQztRQUVsQix1QkFBa0IsR0FBRyxJQUFJLHVDQUFrQixFQUFFLENBQUM7UUFDOUMsaUJBQVksR0FBRyxJQUFJLCtCQUFZLEVBQUUsQ0FBQztRQUVsQyx3QkFBbUIsR0FBRyxJQUFJLHlDQUFtQixFQUFFLENBQUM7UUFDaEQsa0JBQWEsR0FBRyxJQUFJLHFDQUFpQixFQUFFLENBQUM7UUFFeEMsWUFBTyxHQUFHLElBQUksd0JBQVcsRUFBRSxDQUFDO1FBQzVCLG9CQUFlLEdBQUcsSUFBSSxpQ0FBZSxFQUFFLENBQUM7UUFFeEMsaUJBQVksR0FBRyxJQUFJLDJCQUFZLEVBQUUsQ0FBQztRQUNsQyxtQkFBYyxHQUFHLElBQUkseUJBQWMsRUFBRSxDQUFDO0lBQzFDLENBQUM7Q0FBQTtBQWxCRCxrQkFrQkMifQ==