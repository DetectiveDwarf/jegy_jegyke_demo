"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.kaulogin = void 0;
const protractor_1 = require("protractor");
class kaulogin {
    constructor() {
        this.jegykeLoginButton = protractor_1.element(protractor_1.by.cssContainingText('span.ui-button-text', 'Belépés ügyfélkapu azonosítóval'));
        this.kauCustomerPortalButton = protractor_1.element(protractor_1.by.className("btn"));
        this.kauAccountField = protractor_1.element(protractor_1.by.name("felhasznaloNev"));
        this.kauAccountPasswordField = protractor_1.element(protractor_1.by.name("jelszo"));
        this.kauCustomerPortalLoginButton = protractor_1.element(protractor_1.by.buttonText('bejelentkezés'));
    }
}
exports.kaulogin = kaulogin;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoia2F1LWxvZ2luLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiLi4vLi4vcGFnZU9iamVjdHMva2F1LWxvZ2luLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7OztBQUFBLDJDQUFzRDtBQUV0RCxNQUFhLFFBQVE7SUFTakI7UUFFSSxJQUFJLENBQUMsaUJBQWlCLEdBQUcsb0JBQU8sQ0FBQyxlQUFFLENBQUMsaUJBQWlCLENBQUMscUJBQXFCLEVBQUUsaUNBQWlDLENBQUMsQ0FBQyxDQUFDO1FBQ2pILElBQUksQ0FBQyx1QkFBdUIsR0FBRyxvQkFBTyxDQUFDLGVBQUUsQ0FBQyxTQUFTLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQztRQUM1RCxJQUFJLENBQUMsZUFBZSxHQUFHLG9CQUFPLENBQUMsZUFBRSxDQUFDLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFDLENBQUM7UUFDMUQsSUFBSSxDQUFDLHVCQUF1QixHQUFHLG9CQUFPLENBQUMsZUFBRSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDO1FBQzFELElBQUksQ0FBQyw0QkFBNEIsR0FBRyxvQkFBTyxDQUFDLGVBQUUsQ0FBQyxVQUFVLENBQUMsZUFBZSxDQUFDLENBQUMsQ0FBQztJQUNoRixDQUFDO0NBQ0o7QUFqQkQsNEJBaUJDIn0=