"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.KauLoginPage = void 0;
const protractor_1 = require("protractor");
class KauLoginPage {
    constructor() {
        this.usernameField = protractor_1.element(protractor_1.by.css('input[name="felhasznaloNev"]'));
        this.passwordField = protractor_1.element(protractor_1.by.css('input[name="jelszo"]'));
        this.loginButton = protractor_1.element(protractor_1.by.css('button[type="submit"]'));
        this.welcomeText = protractor_1.element(protractor_1.by.tagName('h1'));
        // Emailes bejelentkezés alatti KAÜS login gomb
        this.kauButton = protractor_1.element(protractor_1.by.cssContainingText('span.ui-button-text', 'Belépés ügyfélkapu azonosítóval'));
        // Ügyfélkapu gomb a KAÜS bejelentkezés felületen
        this.ugyfelkapuButton = protractor_1.element(protractor_1.by.id('urn:eksz.gov.hu:1.0:azonositas:kau:1:uk:uidpwd'));
    }
    // TODO A szerepkör érkezzen paraméterként és az alapján válassza ki a felhasználónevet és jelszót
    kauLogin() {
        return __awaiter(this, void 0, void 0, function* () {
            let cookie;
            yield this.usernameField.sendKeys('ibolya.schoffer');
            yield this.passwordField.sendKeys('jelszo12AA');
            yield this.loginButton.click();
            // Manuálisan kell állítani az időzítést mert nem az Angularon belül vagyunk (és a háttérben sok átirányítás történik)
            // Előfordulhat, hogy növelni kell ezt az értéket ha a KAÜ valamiért lassan tölt be
            yield protractor_1.browser.sleep(5000);
            // A KAÜ átirányít a jegyke-qa.sonrisa.hu -ra, kimásoljuk a cookiet és átírjuk a domaint
            /* await browser
               .manage()
               .getCookies()
               .then((cookies) => {
                 cookie = cookies[0];
                 cookie.domain = 'localhost';
               });
             await browser.sleep(5000);*/
            // Visszakapcsoljuk az Angular eseményekre a várakozást
            yield protractor_1.browser.waitForAngularEnabled(true);
            // Visszalépünk az alkalmazásba
            //await browser.get('https://jegyke-qa.sonrisa.hu/');
            // Beállítjuk a cookiet
            //await browser.manage().addCookie(cookie);
            // Átnavigálunk a kezdőoldalra
            yield protractor_1.browser.get('https://jegyke-qa.sonrisa.hu/');
        });
    }
}
exports.KauLoginPage = KauLoginPage;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoia2F1LWxvZ2luLnBvLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiLi4vLi4vcGFnZU9iamVjdHMva2F1LWxvZ2luLnBvLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7OztBQUFBLDJDQUFrRDtBQUdsRCxNQUFhLFlBQVk7SUFBekI7UUFDSSxrQkFBYSxHQUFHLG9CQUFPLENBQUMsZUFBRSxDQUFDLEdBQUcsQ0FBQyw4QkFBOEIsQ0FBQyxDQUFDLENBQUM7UUFDaEUsa0JBQWEsR0FBRyxvQkFBTyxDQUFDLGVBQUUsQ0FBQyxHQUFHLENBQUMsc0JBQXNCLENBQUMsQ0FBQyxDQUFDO1FBQ3hELGdCQUFXLEdBQUcsb0JBQU8sQ0FBQyxlQUFFLENBQUMsR0FBRyxDQUFDLHVCQUF1QixDQUFDLENBQUMsQ0FBQztRQUN2RCxnQkFBVyxHQUFHLG9CQUFPLENBQUMsZUFBRSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDO1FBRXhDLCtDQUErQztRQUMvQyxjQUFTLEdBQUcsb0JBQU8sQ0FBQyxlQUFFLENBQUMsaUJBQWlCLENBQUMscUJBQXFCLEVBQUUsaUNBQWlDLENBQUMsQ0FBQyxDQUFDO1FBRXBHLGlEQUFpRDtRQUNqRCxxQkFBZ0IsR0FBRyxvQkFBTyxDQUFDLGVBQUUsQ0FBQyxFQUFFLENBQUMsZ0RBQWdELENBQUMsQ0FBQyxDQUFDO0lBOEJ4RixDQUFDO0lBNUJHLGtHQUFrRztJQUM1RixRQUFROztZQUNWLElBQUksTUFBK0IsQ0FBQztZQUNwQyxNQUFNLElBQUksQ0FBQyxhQUFhLENBQUMsUUFBUSxDQUFDLGlCQUFpQixDQUFDLENBQUM7WUFDckQsTUFBTSxJQUFJLENBQUMsYUFBYSxDQUFDLFFBQVEsQ0FBQyxZQUFZLENBQUMsQ0FBQztZQUNoRCxNQUFNLElBQUksQ0FBQyxXQUFXLENBQUMsS0FBSyxFQUFFLENBQUM7WUFFL0Isc0hBQXNIO1lBQ3RILG1GQUFtRjtZQUNuRixNQUFNLG9CQUFPLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxDQUFDO1lBQzFCLHdGQUF3RjtZQUN4Rjs7Ozs7Ozt5Q0FPNkI7WUFDN0IsdURBQXVEO1lBQ3ZELE1BQU0sb0JBQU8sQ0FBQyxxQkFBcUIsQ0FBQyxJQUFJLENBQUMsQ0FBQztZQUMxQywrQkFBK0I7WUFDL0IscURBQXFEO1lBQ3JELHVCQUF1QjtZQUN2QiwyQ0FBMkM7WUFDM0MsOEJBQThCO1lBQzlCLE1BQU0sb0JBQU8sQ0FBQyxHQUFHLENBQUMsK0JBQStCLENBQUMsQ0FBQztRQUN2RCxDQUFDO0tBQUE7Q0FDSjtBQXhDRCxvQ0F3Q0MifQ==