"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ProgramListPage = void 0;
const protractor_1 = require("protractor");
const form_po_1 = require("./form.po");
class ProgramListPage {
    constructor() {
        this.ujProgramButton = protractor_1.element(protractor_1.by.partialButtonText('Új program hozzáadása'));
        this.tableRows = protractor_1.element.all(protractor_1.by.css('.divTableRow'));
        this.searchField = protractor_1.element(protractor_1.by.css('.kereses-block input'));
        this.inactiveFilterCheckbox = form_po_1.Form.getCheckboxByName('statuszSzuro', 'Inaktív');
        this.activeFilterCheckbox = form_po_1.Form.getCheckboxByName('statuszSzuro', 'Aktív');
        this.toroltFilterCheckbox = form_po_1.Form.getCheckboxByName('statuszSzuro', 'Törölt');
    }
}
exports.ProgramListPage = ProgramListPage;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicHJvZ3JhbS1saXN0LnBvLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiLi4vLi4vcGFnZU9iamVjdHMvcHJvZ3JhbS1saXN0LnBvLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7OztBQUFBLDJDQUF5QztBQUN6Qyx1Q0FBaUM7QUFFakMsTUFBYSxlQUFlO0lBQTVCO1FBQ0ksb0JBQWUsR0FBRyxvQkFBTyxDQUFDLGVBQUUsQ0FBQyxpQkFBaUIsQ0FBQyx1QkFBdUIsQ0FBQyxDQUFDLENBQUM7UUFDekUsY0FBUyxHQUFHLG9CQUFPLENBQUMsR0FBRyxDQUFDLGVBQUUsQ0FBQyxHQUFHLENBQUMsY0FBYyxDQUFDLENBQUMsQ0FBQztRQUNoRCxnQkFBVyxHQUFHLG9CQUFPLENBQUMsZUFBRSxDQUFDLEdBQUcsQ0FBQyxzQkFBc0IsQ0FBQyxDQUFDLENBQUM7UUFDdEQsMkJBQXNCLEdBQUcsY0FBSSxDQUFDLGlCQUFpQixDQUFDLGNBQWMsRUFBRSxTQUFTLENBQUMsQ0FBQztRQUMzRSx5QkFBb0IsR0FBRyxjQUFJLENBQUMsaUJBQWlCLENBQUMsY0FBYyxFQUFFLE9BQU8sQ0FBQyxDQUFDO1FBQ3ZFLHlCQUFvQixHQUFHLGNBQUksQ0FBQyxpQkFBaUIsQ0FBQyxjQUFjLEVBQUUsUUFBUSxDQUFDLENBQUM7SUFDNUUsQ0FBQztDQUFBO0FBUEQsMENBT0MifQ==