"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.EmailLoginPage = void 0;
const protractor_1 = require("protractor");
const credentials_dto_1 = require("../models/credentials.dto");
const user_type_enum_1 = require("../models/user-type.enum");
const TEST_USER_CREDENTIALS = new Map([
    [user_type_enum_1.UserType.NO_EMAIL, new credentials_dto_1.Credentials('', 'Jelszo1234')],
    [user_type_enum_1.UserType.NO_PASSWORD, new credentials_dto_1.Credentials('test@temp.hu', '')],
    [user_type_enum_1.UserType.WRONG_PASSWORD, new credentials_dto_1.Credentials('test@temp.hu', 'wrong password')],
    [user_type_enum_1.UserType.WRONG_EMAIL, new credentials_dto_1.Credentials('wrong_email@temp.hu', 'user')],
    [user_type_enum_1.UserType.JEGYERTEKESITO, new credentials_dto_1.Credentials('test@temp.hu', 'user')],
    [user_type_enum_1.UserType.JEGYERVENYESITO, new credentials_dto_1.Credentials('test@temp.hu', 'user')],
    [user_type_enum_1.UserType.SZOLGMENEDZSER, new credentials_dto_1.Credentials('test@temp.hu', 'user')],
]);
class EmailLoginPage {
    constructor() {
        this.usernameField = protractor_1.element(protractor_1.by.css('input[formControlName="email"]'));
        this.passwordField = protractor_1.element(protractor_1.by.css('input[formControlName="password"]'));
        this.loginButton = protractor_1.element(protractor_1.by.buttonText('Bejelentkezés'));
    }
    // TODO A szerepkör érkezzen paraméterként és az alapján válassza ki a felhasználónevet és jelszót
    login(role) {
        return __awaiter(this, void 0, void 0, function* () {
            let credentials = TEST_USER_CREDENTIALS.get(role);
            //await this.usernameField.sendKeys(credentials.email);
            //await this.passwordField.sendKeys(credentials.password);
            yield this.loginButton.click();
        });
    }
}
exports.EmailLoginPage = EmailLoginPage;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibG9naW4ucG8uanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyIuLi8uLi9wYWdlT2JqZWN0cy9sb2dpbi5wby50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7QUFBQSwyQ0FBc0Q7QUFDdEQsK0RBQXdEO0FBQ3hELDZEQUFvRDtBQUVwRCxNQUFNLHFCQUFxQixHQUErQixJQUFJLEdBQUcsQ0FBQztJQUM5RCxDQUFDLHlCQUFRLENBQUMsUUFBUSxFQUFFLElBQUksNkJBQVcsQ0FBQyxFQUFFLEVBQUUsWUFBWSxDQUFDLENBQUM7SUFDdEQsQ0FBQyx5QkFBUSxDQUFDLFdBQVcsRUFBRSxJQUFJLDZCQUFXLENBQUMsY0FBYyxFQUFFLEVBQUUsQ0FBQyxDQUFDO0lBQzNELENBQUMseUJBQVEsQ0FBQyxjQUFjLEVBQUUsSUFBSSw2QkFBVyxDQUFDLGNBQWMsRUFBRSxnQkFBZ0IsQ0FBQyxDQUFDO0lBQzVFLENBQUMseUJBQVEsQ0FBQyxXQUFXLEVBQUUsSUFBSSw2QkFBVyxDQUFDLHFCQUFxQixFQUFFLE1BQU0sQ0FBQyxDQUFDO0lBQ3RFLENBQUMseUJBQVEsQ0FBQyxjQUFjLEVBQUUsSUFBSSw2QkFBVyxDQUFDLGNBQWMsRUFBRSxNQUFNLENBQUMsQ0FBQztJQUNsRSxDQUFDLHlCQUFRLENBQUMsZUFBZSxFQUFFLElBQUksNkJBQVcsQ0FBQyxjQUFjLEVBQUUsTUFBTSxDQUFDLENBQUM7SUFDbkUsQ0FBQyx5QkFBUSxDQUFDLGNBQWMsRUFBRSxJQUFJLDZCQUFXLENBQUMsY0FBYyxFQUFFLE1BQU0sQ0FBQyxDQUFDO0NBQ3JFLENBQUMsQ0FBQztBQUVILE1BQWEsY0FBYztJQUt2QjtRQUVJLElBQUksQ0FBQyxhQUFhLEdBQUcsb0JBQU8sQ0FBQyxlQUFFLENBQUMsR0FBRyxDQUFDLGdDQUFnQyxDQUFDLENBQUMsQ0FBQztRQUN2RSxJQUFJLENBQUMsYUFBYSxHQUFHLG9CQUFPLENBQUMsZUFBRSxDQUFDLEdBQUcsQ0FBQyxtQ0FBbUMsQ0FBQyxDQUFDLENBQUM7UUFDMUUsSUFBSSxDQUFDLFdBQVcsR0FBRyxvQkFBTyxDQUFDLGVBQUUsQ0FBQyxVQUFVLENBQUMsZUFBZSxDQUFDLENBQUMsQ0FBQTtJQUM5RCxDQUFDO0lBRUQsa0dBQWtHO0lBRTVGLEtBQUssQ0FBQyxJQUFjOztZQUN0QixJQUFJLFdBQVcsR0FBRyxxQkFBcUIsQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLENBQUM7WUFDbEQsdURBQXVEO1lBQ3ZELDBEQUEwRDtZQUMxRCxNQUFNLElBQUksQ0FBQyxXQUFXLENBQUMsS0FBSyxFQUFFLENBQUM7UUFDbkMsQ0FBQztLQUFBO0NBQ0o7QUFwQkQsd0NBb0JDIn0=