"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ShellPage = void 0;
const protractor_1 = require("protractor");
class ShellPage {
    constructor() {
        this.welcomeText = protractor_1.element(protractor_1.by.css('app-root h1'));
        this.toastMessage = protractor_1.element(protractor_1.by.css('.ui-toast-message'));
    }
    getParagraphText() {
        return this.welcomeText.getText();
    }
}
exports.ShellPage = ShellPage;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic2hlbGwucG8uanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyIuLi8uLi9wYWdlT2JqZWN0cy9zaGVsbC5wby50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7QUFBQSwyQ0FBeUM7QUFFekMsTUFBYSxTQUFTO0lBQXRCO1FBQ0ksZ0JBQVcsR0FBRyxvQkFBTyxDQUFDLGVBQUUsQ0FBQyxHQUFHLENBQUMsYUFBYSxDQUFDLENBQUMsQ0FBQztRQUM3QyxpQkFBWSxHQUFHLG9CQUFPLENBQUMsZUFBRSxDQUFDLEdBQUcsQ0FBQyxtQkFBbUIsQ0FBQyxDQUFDLENBQUM7SUFLeEQsQ0FBQztJQUhHLGdCQUFnQjtRQUNaLE9BQU8sSUFBSSxDQUFDLFdBQVcsQ0FBQyxPQUFPLEVBQUUsQ0FBQztJQUN0QyxDQUFDO0NBQ0o7QUFQRCw4QkFPQyJ9