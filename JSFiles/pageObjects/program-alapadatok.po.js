"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ProgramAlapadatokPage = void 0;
const protractor_1 = require("protractor");
const form_po_1 = require("./form.po");
class ProgramAlapadatokPage {
    constructor() {
        this.normalProgramRadioButton = form_po_1.Form.getRadioButton('programTipus', 'Normál program');
        this.kombinaltProgramRadioButton = form_po_1.Form.getRadioButton('programTipus', 'Kombinált program');
        this.programNevField = form_po_1.Form.getInput('hivatalosNev');
        this.idegenNevField = form_po_1.Form.getInput('angolNev');
        this.gyakorisagDropdown = 'gyakorisag';
        this.programKezdeteDate = form_po_1.Form.getDateInput('kezdete');
        this.programVegeDate = form_po_1.Form.getDateInput('vege');
        this.jegyertekesitesKezdeteDate = form_po_1.Form.getDateInput('jegyertekesitesKezdete');
        this.jegyertekesitesVegeDate = form_po_1.Form.getDateInput('jegyertekesitesVege');
        this.jegyertekesitesMegegyezikRadioButton = form_po_1.Form.getRadioButton('jegyertekesitesElteroIdovel', 'A programmal megegyező');
        this.jegyertekesitesElterRadioButton = form_po_1.Form.getRadioButton('jegyertekesitesElteroIdovel', 'A programtól eltérő');
        this.fokategoriaDropdown = 'programFokategoria';
        this.alkategoriaDropdown = 'programAlkategoria';
        this.helyszinDropdown = 'helyszin';
        this.jegyRendszerAzonosizoField = form_po_1.Form.getInput('jegyRendszerProgramazonosito');
        this.leirasField = form_po_1.Form.getTextarea('leiras');
        this.weboldalField = form_po_1.Form.getTextarea('url');
        this.ujKapcsolodoProgramHozzaadasaButton = protractor_1.element(protractor_1.by.partialButtonText('Új program hozzáadása'));
        this.ujKapcsolodoProgramDropdown = 'hozzaadottProgram';
        this.hozzaAdasButton = protractor_1.element(protractor_1.by.partialButtonText('hozzáadás'));
        this.statusAktivText = protractor_1.element(protractor_1.by.css('.statusz.AKTIV'));
        this.statusInaktivText = protractor_1.element(protractor_1.by.css('.statusz.INAKTIV'));
    }
}
exports.ProgramAlapadatokPage = ProgramAlapadatokPage;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicHJvZ3JhbS1hbGFwYWRhdG9rLnBvLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiLi4vLi4vcGFnZU9iamVjdHMvcHJvZ3JhbS1hbGFwYWRhdG9rLnBvLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7OztBQUFBLDJDQUF5QztBQUN6Qyx1Q0FBaUM7QUFFakMsTUFBYSxxQkFBcUI7SUFBbEM7UUFDSSw2QkFBd0IsR0FBRyxjQUFJLENBQUMsY0FBYyxDQUFDLGNBQWMsRUFBRSxnQkFBZ0IsQ0FBQyxDQUFDO1FBQ2pGLGdDQUEyQixHQUFHLGNBQUksQ0FBQyxjQUFjLENBQUMsY0FBYyxFQUFFLG1CQUFtQixDQUFDLENBQUM7UUFDdkYsb0JBQWUsR0FBRyxjQUFJLENBQUMsUUFBUSxDQUFDLGNBQWMsQ0FBQyxDQUFDO1FBQ2hELG1CQUFjLEdBQUcsY0FBSSxDQUFDLFFBQVEsQ0FBQyxVQUFVLENBQUMsQ0FBQztRQUMzQyx1QkFBa0IsR0FBRyxZQUFZLENBQUM7UUFDbEMsdUJBQWtCLEdBQUcsY0FBSSxDQUFDLFlBQVksQ0FBQyxTQUFTLENBQUMsQ0FBQztRQUNsRCxvQkFBZSxHQUFHLGNBQUksQ0FBQyxZQUFZLENBQUMsTUFBTSxDQUFDLENBQUM7UUFDNUMsK0JBQTBCLEdBQUcsY0FBSSxDQUFDLFlBQVksQ0FBQyx3QkFBd0IsQ0FBQyxDQUFDO1FBQ3pFLDRCQUF1QixHQUFHLGNBQUksQ0FBQyxZQUFZLENBQUMscUJBQXFCLENBQUMsQ0FBQztRQUNuRSx5Q0FBb0MsR0FBRyxjQUFJLENBQUMsY0FBYyxDQUFDLDZCQUE2QixFQUFFLHdCQUF3QixDQUFDLENBQUM7UUFDcEgsb0NBQStCLEdBQUcsY0FBSSxDQUFDLGNBQWMsQ0FBQyw2QkFBNkIsRUFBRSxxQkFBcUIsQ0FBQyxDQUFDO1FBQzVHLHdCQUFtQixHQUFHLG9CQUFvQixDQUFDO1FBQzNDLHdCQUFtQixHQUFHLG9CQUFvQixDQUFDO1FBQzNDLHFCQUFnQixHQUFHLFVBQVUsQ0FBQztRQUM5QiwrQkFBMEIsR0FBRyxjQUFJLENBQUMsUUFBUSxDQUFDLDhCQUE4QixDQUFDLENBQUM7UUFDM0UsZ0JBQVcsR0FBRyxjQUFJLENBQUMsV0FBVyxDQUFDLFFBQVEsQ0FBQyxDQUFDO1FBQ3pDLGtCQUFhLEdBQUcsY0FBSSxDQUFDLFdBQVcsQ0FBQyxLQUFLLENBQUMsQ0FBQztRQUN4Qyx3Q0FBbUMsR0FBRyxvQkFBTyxDQUFDLGVBQUUsQ0FBQyxpQkFBaUIsQ0FBQyx1QkFBdUIsQ0FBQyxDQUFDLENBQUM7UUFDN0YsZ0NBQTJCLEdBQUcsbUJBQW1CLENBQUM7UUFDbEQsb0JBQWUsR0FBRyxvQkFBTyxDQUFDLGVBQUUsQ0FBQyxpQkFBaUIsQ0FBQyxXQUFXLENBQUMsQ0FBQyxDQUFDO1FBQzdELG9CQUFlLEdBQUcsb0JBQU8sQ0FBQyxlQUFFLENBQUMsR0FBRyxDQUFDLGdCQUFnQixDQUFDLENBQUMsQ0FBQztRQUNwRCxzQkFBaUIsR0FBRyxvQkFBTyxDQUFDLGVBQUUsQ0FBQyxHQUFHLENBQUMsa0JBQWtCLENBQUMsQ0FBQyxDQUFDO0lBQzVELENBQUM7Q0FBQTtBQXZCRCxzREF1QkMifQ==