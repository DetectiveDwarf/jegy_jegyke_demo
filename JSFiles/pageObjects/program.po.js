"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ProgramPage = void 0;
const protractor_1 = require("protractor");
const program_alapadatok_po_1 = require("./program-alapadatok.po");
const program_jegytipusok_es_alkalmak_po_1 = require("./program-jegytipusok-es-alkalmak.po");
class ProgramPage {
    constructor() {
        // Tabs
        this.alapadatokTab = protractor_1.element(protractor_1.by.buttonText('Alapadatok'));
        this.jegytipusokEsAlkalmakTab = protractor_1.element(protractor_1.by.buttonText('Jegytípusok és alkalmak'));
        this.kajutTab = protractor_1.element(protractor_1.by.buttonText('KAJÜT adatok'));
        // Activity buttons
        this.activateButton = protractor_1.element(protractor_1.by.partialButtonText('Program aktiválása'));
        this.inctivateButton = protractor_1.element(protractor_1.by.partialButtonText('Program inaktiválása'));
        this.deleteButton = protractor_1.element(protractor_1.by.partialButtonText('Program törlése'));
        this.alapadatok = new program_alapadatok_po_1.ProgramAlapadatokPage();
        this.jegytipusokEsAlkalmak = new program_jegytipusok_es_alkalmak_po_1.JegytipusokEsAlkalmakPage();
    }
}
exports.ProgramPage = ProgramPage;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicHJvZ3JhbS5wby5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIi4uLy4uL3BhZ2VPYmplY3RzL3Byb2dyYW0ucG8udHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7O0FBQUEsMkNBQXlDO0FBQ3pDLG1FQUFnRTtBQUNoRSw2RkFBaUY7QUFFakYsTUFBYSxXQUFXO0lBQXhCO1FBQ0ksT0FBTztRQUNQLGtCQUFhLEdBQUcsb0JBQU8sQ0FBQyxlQUFFLENBQUMsVUFBVSxDQUFDLFlBQVksQ0FBQyxDQUFDLENBQUM7UUFDckQsNkJBQXdCLEdBQUcsb0JBQU8sQ0FBQyxlQUFFLENBQUMsVUFBVSxDQUFDLHlCQUF5QixDQUFDLENBQUMsQ0FBQztRQUM3RSxhQUFRLEdBQUcsb0JBQU8sQ0FBQyxlQUFFLENBQUMsVUFBVSxDQUFDLGNBQWMsQ0FBQyxDQUFDLENBQUM7UUFFbEQsbUJBQW1CO1FBQ25CLG1CQUFjLEdBQUcsb0JBQU8sQ0FBQyxlQUFFLENBQUMsaUJBQWlCLENBQUMsb0JBQW9CLENBQUMsQ0FBQyxDQUFDO1FBQ3JFLG9CQUFlLEdBQUcsb0JBQU8sQ0FBQyxlQUFFLENBQUMsaUJBQWlCLENBQUMsc0JBQXNCLENBQUMsQ0FBQyxDQUFDO1FBQ3hFLGlCQUFZLEdBQUcsb0JBQU8sQ0FBQyxlQUFFLENBQUMsaUJBQWlCLENBQUMsaUJBQWlCLENBQUMsQ0FBQyxDQUFDO1FBRWhFLGVBQVUsR0FBRyxJQUFJLDZDQUFxQixFQUFFLENBQUM7UUFDekMsMEJBQXFCLEdBQUcsSUFBSSw4REFBeUIsRUFBRSxDQUFDO0lBQzVELENBQUM7Q0FBQTtBQWJELGtDQWFDIn0=