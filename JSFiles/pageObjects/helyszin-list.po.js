"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.HelyszinPage = void 0;
const protractor_1 = require("protractor");
class HelyszinPage {
    constructor() {
        this.tableRows = protractor_1.element.all(protractor_1.by.css('.divTableRow'));
        this.ujHelyszinButton = protractor_1.element(protractor_1.by.buttonText('Új programhelyszín hozzáadása'));
        this.tableRows = protractor_1.element.all(protractor_1.by.css('.divTableRow'));
    }
}
exports.HelyszinPage = HelyszinPage;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaGVseXN6aW4tbGlzdC5wby5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIi4uLy4uL3BhZ2VPYmplY3RzL2hlbHlzemluLWxpc3QucG8udHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7O0FBQUEsMkNBQXNEO0FBRXRELE1BQWEsWUFBWTtJQUlyQjtRQUZBLGNBQVMsR0FBRyxvQkFBTyxDQUFDLEdBQUcsQ0FBQyxlQUFFLENBQUMsR0FBRyxDQUFDLGNBQWMsQ0FBQyxDQUFDLENBQUM7UUFJNUMsSUFBSSxDQUFDLGdCQUFnQixHQUFHLG9CQUFPLENBQUMsZUFBRSxDQUFDLFVBQVUsQ0FBQywrQkFBK0IsQ0FBQyxDQUFDLENBQUM7UUFDaEYsSUFBSSxDQUFDLFNBQVMsR0FBRyxvQkFBTyxDQUFDLEdBQUcsQ0FBQyxlQUFFLENBQUMsR0FBRyxDQUFDLGNBQWMsQ0FBQyxDQUFDLENBQUM7SUFDekQsQ0FBQztDQUVKO0FBVkQsb0NBVUMifQ==