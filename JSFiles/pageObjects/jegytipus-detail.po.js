"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.JegytipusDetailPage = void 0;
const protractor_1 = require("protractor");
const form_po_1 = require("./form.po");
const app_po_1 = require("./app.po");
class JegytipusDetailPage {
    constructor() {
        this.megnevezesField = form_po_1.Form.getInput('megnevezes');
        this.rovidKodField = form_po_1.Form.getInput('rovidKod');
        this.rovidLeirasField = form_po_1.Form.getInput('leiras');
        // Személyek száma
        this.szemelyekSzamaRadioButton = 'szemelyekSzama';
        this.szemelyekSzamaField = form_po_1.Form.getInput('csoportosSzemelyekNum');
        // Belépések száma
        this.belepesekSzamaRadioButton = 'belepesekSzama';
        this.belepesekSzamaField = form_po_1.Form.getInput('korlatozottBelepesekSzamaNum');
        // Felhasználható
        this.felhasznalhatoRadioButton = 'felhasznalhato';
        this.felhasznalhatoField = form_po_1.Form.getInput('felhasznalhatoNum');
        this.jegyArTipusaDropdown = 'arkepzes';
        this.korcsoportDropdown = 'korcsoport';
        this.jegytipusKategoriaDropdown = 'jegyrendszerKategoria';
        this.ujkedvezmenyButton = protractor_1.element(protractor_1.by.buttonText('Új kedvezmény rögzítése'));
        this.kedvezmenyDropdown = 'kedvezmeny';
        this.hozzaadasButton = protractor_1.element(protractor_1.by.buttonText('Hozzáadás'));
        this.kedvezmenyTorlesButton = protractor_1.element(protractor_1.by.css('.pi-times'));
        // Activity functions
        this.activateButton = protractor_1.element(protractor_1.by.partialButtonText('Jegytípus aktiválása'));
        this.inactivateButton = protractor_1.element(protractor_1.by.partialButtonText('Jegytípus inaktiválása'));
        this.confirmButton = protractor_1.element(protractor_1.by.partialButtonText('Megerősítés'));
        this.closeButton = protractor_1.element(protractor_1.by.partialButtonText('Bezárás'));
        // Form actions
        this.modositasokElveteseButton = protractor_1.element(protractor_1.by.buttonText('Módosítások elvetése'));
        this.mentesButton = protractor_1.element(protractor_1.by.buttonText('Mentés'));
        this.bezarasButton = protractor_1.element(protractor_1.by.buttonText('Bezárás'));
    }
    createJegytipus(jegytipusName) {
        return __awaiter(this, void 0, void 0, function* () {
            const form = new form_po_1.Form();
            const app = new app_po_1.App();
            yield this.megnevezesField.sendKeys(jegytipusName);
            yield form_po_1.Form.selectDropdownItem(app.jegytipusDetailPage.korcsoportDropdown, 'Felnőtt (25-63)');
            yield form_po_1.Form.selectDropdownItem(app.jegytipusDetailPage.jegytipusKategoriaDropdown, 'Egyéni');
            yield form.mentesButton.click();
        });
    }
}
exports.JegytipusDetailPage = JegytipusDetailPage;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiamVneXRpcHVzLWRldGFpbC5wby5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIi4uLy4uL3BhZ2VPYmplY3RzL2plZ3l0aXB1cy1kZXRhaWwucG8udHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7O0FBQUEsMkNBQXlDO0FBQ3pDLHVDQUFpQztBQUNqQyxxQ0FBK0I7QUFFL0IsTUFBYSxtQkFBbUI7SUFBaEM7UUFDSSxvQkFBZSxHQUFHLGNBQUksQ0FBQyxRQUFRLENBQUMsWUFBWSxDQUFDLENBQUM7UUFDOUMsa0JBQWEsR0FBRyxjQUFJLENBQUMsUUFBUSxDQUFDLFVBQVUsQ0FBQyxDQUFDO1FBQzFDLHFCQUFnQixHQUFHLGNBQUksQ0FBQyxRQUFRLENBQUMsUUFBUSxDQUFDLENBQUM7UUFFM0Msa0JBQWtCO1FBQ2xCLDhCQUF5QixHQUFHLGdCQUFnQixDQUFDO1FBQzdDLHdCQUFtQixHQUFHLGNBQUksQ0FBQyxRQUFRLENBQUMsdUJBQXVCLENBQUMsQ0FBQztRQUU3RCxrQkFBa0I7UUFDbEIsOEJBQXlCLEdBQUcsZ0JBQWdCLENBQUM7UUFDN0Msd0JBQW1CLEdBQUcsY0FBSSxDQUFDLFFBQVEsQ0FBQyw4QkFBOEIsQ0FBQyxDQUFDO1FBRXBFLGlCQUFpQjtRQUNqQiw4QkFBeUIsR0FBRyxnQkFBZ0IsQ0FBQztRQUM3Qyx3QkFBbUIsR0FBRyxjQUFJLENBQUMsUUFBUSxDQUFDLG1CQUFtQixDQUFDLENBQUM7UUFFekQseUJBQW9CLEdBQUcsVUFBVSxDQUFDO1FBQ2xDLHVCQUFrQixHQUFHLFlBQVksQ0FBQztRQUNsQywrQkFBMEIsR0FBRyx1QkFBdUIsQ0FBQztRQUNyRCx1QkFBa0IsR0FBRyxvQkFBTyxDQUFDLGVBQUUsQ0FBQyxVQUFVLENBQUMseUJBQXlCLENBQUMsQ0FBQyxDQUFDO1FBQ3ZFLHVCQUFrQixHQUFHLFlBQVksQ0FBQztRQUNsQyxvQkFBZSxHQUFHLG9CQUFPLENBQUMsZUFBRSxDQUFDLFVBQVUsQ0FBQyxXQUFXLENBQUMsQ0FBQyxDQUFDO1FBQ3RELDJCQUFzQixHQUFHLG9CQUFPLENBQUMsZUFBRSxDQUFDLEdBQUcsQ0FBQyxXQUFXLENBQUMsQ0FBQyxDQUFDO1FBRXRELHFCQUFxQjtRQUNyQixtQkFBYyxHQUFHLG9CQUFPLENBQUMsZUFBRSxDQUFDLGlCQUFpQixDQUFDLHNCQUFzQixDQUFDLENBQUMsQ0FBQztRQUN2RSxxQkFBZ0IsR0FBRyxvQkFBTyxDQUFDLGVBQUUsQ0FBQyxpQkFBaUIsQ0FBQyx3QkFBd0IsQ0FBQyxDQUFDLENBQUM7UUFDM0Usa0JBQWEsR0FBRyxvQkFBTyxDQUFDLGVBQUUsQ0FBQyxpQkFBaUIsQ0FBQyxhQUFhLENBQUMsQ0FBQyxDQUFDO1FBQzdELGdCQUFXLEdBQUcsb0JBQU8sQ0FBQyxlQUFFLENBQUMsaUJBQWlCLENBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQztRQUV2RCxlQUFlO1FBQ2YsOEJBQXlCLEdBQUcsb0JBQU8sQ0FBQyxlQUFFLENBQUMsVUFBVSxDQUFDLHNCQUFzQixDQUFDLENBQUMsQ0FBQztRQUMzRSxpQkFBWSxHQUFHLG9CQUFPLENBQUMsZUFBRSxDQUFDLFVBQVUsQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDO1FBQ2hELGtCQUFhLEdBQUcsb0JBQU8sQ0FBQyxlQUFFLENBQUMsVUFBVSxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUM7SUFVdEQsQ0FBQztJQVJTLGVBQWUsQ0FBQyxhQUFxQjs7WUFDdkMsTUFBTSxJQUFJLEdBQUcsSUFBSSxjQUFJLEVBQUUsQ0FBQztZQUN4QixNQUFNLEdBQUcsR0FBRyxJQUFJLFlBQUcsRUFBRSxDQUFDO1lBQ3RCLE1BQU0sSUFBSSxDQUFDLGVBQWUsQ0FBQyxRQUFRLENBQUMsYUFBYSxDQUFDLENBQUM7WUFDbkQsTUFBTSxjQUFJLENBQUMsa0JBQWtCLENBQUMsR0FBRyxDQUFDLG1CQUFtQixDQUFDLGtCQUFrQixFQUFFLGlCQUFpQixDQUFDLENBQUM7WUFDN0YsTUFBTSxjQUFJLENBQUMsa0JBQWtCLENBQUMsR0FBRyxDQUFDLG1CQUFtQixDQUFDLDBCQUEwQixFQUFFLFFBQVEsQ0FBQyxDQUFDO1lBQzVGLE1BQU0sSUFBSSxDQUFDLFlBQVksQ0FBQyxLQUFLLEVBQUUsQ0FBQztRQUNwQyxDQUFDO0tBQUE7Q0FDSjtBQTVDRCxrREE0Q0MifQ==