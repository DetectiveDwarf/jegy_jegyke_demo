"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.MenuPage = void 0;
const protractor_1 = require("protractor");
class MenuPage {
    constructor() {
        this.userMenuButton = protractor_1.element(protractor_1.by.css('button[type="button"].user-icon'));
        this.kilepesUserMenuButton = protractor_1.element(protractor_1.by.cssContainingText('span.ui-menuitem-text', 'Kilépés'));
        this.beallitasokMenu = protractor_1.element(protractor_1.by.cssContainingText('span[class="ui-menuitem-text"]', 'Beállítások'));
        this.jegytipusMenu = protractor_1.element(protractor_1.by.cssContainingText('span[class="ui-menuitem-text"]', 'Jegytípusok'));
        this.helyszinMenu = protractor_1.element(protractor_1.by.cssContainingText('span[class="ui-menuitem-text"]', 'Programhelyszínek'));
        this.programMenu = protractor_1.element(protractor_1.by.cssContainingText('span[class="ui-menuitem-text"]', 'Programok'));
    }
    goToJegytipusok() {
        return __awaiter(this, void 0, void 0, function* () {
            yield this.beallitasokMenu.click();
            yield this.jegytipusMenu.click();
        });
    }
    logout() {
        return __awaiter(this, void 0, void 0, function* () {
            yield this.userMenuButton.click();
            yield this.kilepesUserMenuButton.click();
        });
    }
    goToHelyszinek() {
        return __awaiter(this, void 0, void 0, function* () {
            yield this.beallitasokMenu.click();
            yield this.helyszinMenu.click();
        });
    }
}
exports.MenuPage = MenuPage;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibWVudS5wby5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIi4uLy4uL3BhZ2VPYmplY3RzL21lbnUucG8udHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7O0FBQUEsMkNBQStEO0FBRy9ELE1BQWEsUUFBUTtJQWtCakI7UUFFSSxJQUFJLENBQUMsY0FBYyxHQUFHLG9CQUFPLENBQUMsZUFBRSxDQUFDLEdBQUcsQ0FBQyxpQ0FBaUMsQ0FBQyxDQUFDLENBQUM7UUFDekUsSUFBSSxDQUFDLHFCQUFxQixHQUFHLG9CQUFPLENBQUMsZUFBRSxDQUFDLGlCQUFpQixDQUFDLHVCQUF1QixFQUFFLFNBQVMsQ0FBQyxDQUFDLENBQUM7UUFDL0YsSUFBSSxDQUFDLGVBQWUsR0FBRyxvQkFBTyxDQUFDLGVBQUUsQ0FBQyxpQkFBaUIsQ0FBQyxnQ0FBZ0MsRUFBRSxhQUFhLENBQUMsQ0FBQyxDQUFDO1FBQ3RHLElBQUksQ0FBQyxhQUFhLEdBQUcsb0JBQU8sQ0FBQyxlQUFFLENBQUMsaUJBQWlCLENBQUMsZ0NBQWdDLEVBQUUsYUFBYSxDQUFDLENBQUMsQ0FBQztRQUNwRyxJQUFJLENBQUMsWUFBWSxHQUFHLG9CQUFPLENBQUMsZUFBRSxDQUFDLGlCQUFpQixDQUFDLGdDQUFnQyxFQUFFLG1CQUFtQixDQUFDLENBQUMsQ0FBQztRQUN6RyxJQUFJLENBQUMsV0FBVyxHQUFHLG9CQUFPLENBQUMsZUFBRSxDQUFDLGlCQUFpQixDQUFDLGdDQUFnQyxFQUFFLFdBQVcsQ0FBQyxDQUFDLENBQUM7SUFDcEcsQ0FBQztJQUVLLGVBQWU7O1lBQ2pCLE1BQU0sSUFBSSxDQUFDLGVBQWUsQ0FBQyxLQUFLLEVBQUUsQ0FBQztZQUNuQyxNQUFNLElBQUksQ0FBQyxhQUFhLENBQUMsS0FBSyxFQUFFLENBQUM7UUFDckMsQ0FBQztLQUFBO0lBRUssTUFBTTs7WUFDUixNQUFNLElBQUksQ0FBQyxjQUFjLENBQUMsS0FBSyxFQUFFLENBQUM7WUFDbEMsTUFBTSxJQUFJLENBQUMscUJBQXFCLENBQUMsS0FBSyxFQUFFLENBQUM7UUFDN0MsQ0FBQztLQUFBO0lBRUssY0FBYzs7WUFDaEIsTUFBTSxJQUFJLENBQUMsZUFBZSxDQUFDLEtBQUssRUFBRSxDQUFDO1lBQ25DLE1BQU0sSUFBSSxDQUFDLFlBQVksQ0FBQyxLQUFLLEVBQUUsQ0FBQztRQUNwQyxDQUFDO0tBQUE7Q0FDSjtBQTFDRCw0QkEwQ0MifQ==