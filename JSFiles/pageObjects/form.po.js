"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Form = void 0;
const protractor_1 = require("protractor");
class Form {
    constructor() {
        // Form actions
        this.modositasokElveteseButton = protractor_1.element(protractor_1.by.partialButtonText('Módosítások elvetése'));
        this.mentesButton = protractor_1.element(protractor_1.by.partialButtonText('Mentés'));
        this.bezarasButton = protractor_1.element(protractor_1.by.partialButtonText('Bezárás'));
    }
    static getInput(formControlName) {
        return protractor_1.element(protractor_1.by.css('input[formcontrolname=' + formControlName + ']'));
    }
    static getTextarea(formControlName) {
        return protractor_1.element(protractor_1.by.css('textarea[formcontrolname=' + formControlName + ']'));
    }
    static getInputMask(formControlName) {
        return protractor_1.element(protractor_1.by.css('p-inputmask[formcontrolname=' + formControlName + '] input'));
    }
    static getAutocomplete(formControlName) {
        return protractor_1.element(protractor_1.by.css('p-autocomplete[formcontrolname=' + formControlName + '] input'));
    }
    static getDropDown(formControlName) {
        return protractor_1.element(protractor_1.by.css('p-dropdown[formcontrolname=' + formControlName + ']'));
    }
    static getDropDownItem(itemName) {
        return protractor_1.element(protractor_1.by.cssContainingText('li span', itemName));
    }
    static selectDropdownItem(formControlName, itemName) {
        return __awaiter(this, void 0, void 0, function* () {
            yield this.getDropDown(formControlName).click();
            yield this.getDropDownItem(itemName).click();
        });
    }
    static getRadioButton(formControlName, label) {
        return protractor_1.element(protractor_1.by.cssContainingText('p-radiobutton[formcontrolname=' + formControlName + '] label', label));
    }
    static selectRadioButton(formControlName, label) {
        return __awaiter(this, void 0, void 0, function* () {
            yield this.getRadioButton(formControlName, label).click();
        });
    }
    static getCheckboxByName(name, label) {
        return protractor_1.element(protractor_1.by.cssContainingText('p-checkbox[name=' + name + '] label', label));
    }
    static getCheckboxByFormControlName(formcontrolname) {
        return protractor_1.element(protractor_1.by.css('p-checkbox[formcontrolname=' + formcontrolname + ']'));
    }
    static getDateInput(formControlName) {
        return protractor_1.element(protractor_1.by.css('app-custom-datepicker[formcontrolname=' + formControlName + '] input'));
    }
}
exports.Form = Form;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZm9ybS5wby5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIi4uLy4uL3BhZ2VPYmplY3RzL2Zvcm0ucG8udHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7O0FBQUEsMkNBQXdEO0FBRXhELE1BQWEsSUFBSTtJQUFqQjtRQUNJLGVBQWU7UUFDZiw4QkFBeUIsR0FBRyxvQkFBTyxDQUFDLGVBQUUsQ0FBQyxpQkFBaUIsQ0FBQyxzQkFBc0IsQ0FBQyxDQUFDLENBQUM7UUFDbEYsaUJBQVksR0FBRyxvQkFBTyxDQUFDLGVBQUUsQ0FBQyxpQkFBaUIsQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDO1FBQ3ZELGtCQUFhLEdBQUcsb0JBQU8sQ0FBQyxlQUFFLENBQUMsaUJBQWlCLENBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQztJQWtEN0QsQ0FBQztJQWhEVSxNQUFNLENBQUMsUUFBUSxDQUFDLGVBQXVCO1FBQzFDLE9BQU8sb0JBQU8sQ0FBQyxlQUFFLENBQUMsR0FBRyxDQUFDLHdCQUF3QixHQUFHLGVBQWUsR0FBRyxHQUFHLENBQUMsQ0FBQyxDQUFDO0lBQzdFLENBQUM7SUFFTSxNQUFNLENBQUMsV0FBVyxDQUFDLGVBQXVCO1FBQzdDLE9BQU8sb0JBQU8sQ0FBQyxlQUFFLENBQUMsR0FBRyxDQUFDLDJCQUEyQixHQUFHLGVBQWUsR0FBRyxHQUFHLENBQUMsQ0FBQyxDQUFDO0lBQ2hGLENBQUM7SUFFTSxNQUFNLENBQUMsWUFBWSxDQUFDLGVBQXVCO1FBQzlDLE9BQU8sb0JBQU8sQ0FBQyxlQUFFLENBQUMsR0FBRyxDQUFDLDhCQUE4QixHQUFHLGVBQWUsR0FBRyxTQUFTLENBQUMsQ0FBQyxDQUFDO0lBQ3pGLENBQUM7SUFFTSxNQUFNLENBQUMsZUFBZSxDQUFDLGVBQXVCO1FBQ2pELE9BQU8sb0JBQU8sQ0FBQyxlQUFFLENBQUMsR0FBRyxDQUFDLGlDQUFpQyxHQUFHLGVBQWUsR0FBRyxTQUFTLENBQUMsQ0FBQyxDQUFDO0lBQzVGLENBQUM7SUFFTSxNQUFNLENBQUMsV0FBVyxDQUFDLGVBQXVCO1FBQzdDLE9BQU8sb0JBQU8sQ0FBQyxlQUFFLENBQUMsR0FBRyxDQUFDLDZCQUE2QixHQUFHLGVBQWUsR0FBRyxHQUFHLENBQUMsQ0FBQyxDQUFDO0lBQ2xGLENBQUM7SUFFTyxNQUFNLENBQUMsZUFBZSxDQUFDLFFBQWdCO1FBQzNDLE9BQU8sb0JBQU8sQ0FBQyxlQUFFLENBQUMsaUJBQWlCLENBQUMsU0FBUyxFQUFFLFFBQVEsQ0FBQyxDQUFDLENBQUM7SUFDOUQsQ0FBQztJQUVNLE1BQU0sQ0FBTyxrQkFBa0IsQ0FBQyxlQUF1QixFQUFFLFFBQWdCOztZQUM1RSxNQUFNLElBQUksQ0FBQyxXQUFXLENBQUMsZUFBZSxDQUFDLENBQUMsS0FBSyxFQUFFLENBQUM7WUFDaEQsTUFBTSxJQUFJLENBQUMsZUFBZSxDQUFDLFFBQVEsQ0FBQyxDQUFDLEtBQUssRUFBRSxDQUFDO1FBQ2pELENBQUM7S0FBQTtJQUVNLE1BQU0sQ0FBQyxjQUFjLENBQUMsZUFBdUIsRUFBRSxLQUFhO1FBQy9ELE9BQU8sb0JBQU8sQ0FBQyxlQUFFLENBQUMsaUJBQWlCLENBQUMsZ0NBQWdDLEdBQUcsZUFBZSxHQUFHLFNBQVMsRUFBRSxLQUFLLENBQUMsQ0FBQyxDQUFDO0lBQ2hILENBQUM7SUFFTSxNQUFNLENBQU8saUJBQWlCLENBQUMsZUFBdUIsRUFBRSxLQUFhOztZQUN4RSxNQUFNLElBQUksQ0FBQyxjQUFjLENBQUMsZUFBZSxFQUFFLEtBQUssQ0FBQyxDQUFDLEtBQUssRUFBRSxDQUFDO1FBQzlELENBQUM7S0FBQTtJQUVNLE1BQU0sQ0FBQyxpQkFBaUIsQ0FBQyxJQUFZLEVBQUUsS0FBYTtRQUN2RCxPQUFPLG9CQUFPLENBQUMsZUFBRSxDQUFDLGlCQUFpQixDQUFDLGtCQUFrQixHQUFHLElBQUksR0FBRyxTQUFTLEVBQUUsS0FBSyxDQUFDLENBQUMsQ0FBQztJQUN2RixDQUFDO0lBRU0sTUFBTSxDQUFDLDRCQUE0QixDQUFDLGVBQXVCO1FBQzlELE9BQU8sb0JBQU8sQ0FBQyxlQUFFLENBQUMsR0FBRyxDQUFDLDZCQUE2QixHQUFHLGVBQWUsR0FBRyxHQUFHLENBQUMsQ0FBQyxDQUFDO0lBQ2xGLENBQUM7SUFFTSxNQUFNLENBQUMsWUFBWSxDQUFDLGVBQXVCO1FBQzlDLE9BQU8sb0JBQU8sQ0FBQyxlQUFFLENBQUMsR0FBRyxDQUFDLHdDQUF3QyxHQUFHLGVBQWUsR0FBRyxTQUFTLENBQUMsQ0FBQyxDQUFDO0lBQ25HLENBQUM7Q0FDSjtBQXRERCxvQkFzREMifQ==