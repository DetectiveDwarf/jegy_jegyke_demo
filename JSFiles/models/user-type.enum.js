"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.UserType = void 0;
var UserType;
(function (UserType) {
    UserType[UserType["NO_EMAIL"] = 0] = "NO_EMAIL";
    UserType[UserType["NO_PASSWORD"] = 1] = "NO_PASSWORD";
    UserType[UserType["WRONG_PASSWORD"] = 2] = "WRONG_PASSWORD";
    UserType[UserType["WRONG_EMAIL"] = 3] = "WRONG_EMAIL";
    UserType[UserType["JEGYERTEKESITO"] = 4] = "JEGYERTEKESITO";
    UserType[UserType["JEGYERVENYESITO"] = 5] = "JEGYERVENYESITO";
    UserType[UserType["SZOLGMENEDZSER"] = 6] = "SZOLGMENEDZSER";
})(UserType = exports.UserType || (exports.UserType = {}));
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidXNlci10eXBlLmVudW0uanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyIuLi8uLi9tb2RlbHMvdXNlci10eXBlLmVudW0udHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7O0FBQUEsSUFBWSxRQVFYO0FBUkQsV0FBWSxRQUFRO0lBQ2hCLCtDQUFRLENBQUE7SUFDUixxREFBVyxDQUFBO0lBQ1gsMkRBQWMsQ0FBQTtJQUNkLHFEQUFXLENBQUE7SUFDWCwyREFBYyxDQUFBO0lBQ2QsNkRBQWUsQ0FBQTtJQUNmLDJEQUFjLENBQUE7QUFDbEIsQ0FBQyxFQVJXLFFBQVEsR0FBUixnQkFBUSxLQUFSLGdCQUFRLFFBUW5CIn0=