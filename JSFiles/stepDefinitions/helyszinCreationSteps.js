"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const cucumber_1 = require("cucumber");
const protractor_1 = require("protractor");
const app_po_1 = require("../pageObjects/app.po");
const login_po_1 = require("../pageObjects/login.po");
let app = new app_po_1.App();
let login = new login_po_1.EmailLoginPage();
cucumber_1.When('I authenticate with {string} and {string}', (string, string2) => __awaiter(void 0, void 0, void 0, function* () {
    // Write code here that turns the phrase above into concrete actions
    yield login.usernameField.sendKeys('test@temp.hu');
    yield login.passwordField.sendKeys('user');
    yield login.loginButton.click();
}));
cucumber_1.When('I navigate to Programhelyszínek page', () => __awaiter(void 0, void 0, void 0, function* () {
    // Write code here that turns the phrase above into concrete actions
    yield protractor_1.browser.sleep(6000);
    yield app.menu.goToHelyszinek();
}));
cucumber_1.When('I click on Új programhelyszín hozzáadása button', () => __awaiter(void 0, void 0, void 0, function* () {
    // Write code here that turns the phrase above into concrete actions
    yield app.helyszinPage.ujHelyszinButton.click();
}));
cucumber_1.When('I fill the mandatory fields with valid data on Programhelyszín létrehozása page', () => __awaiter(void 0, void 0, void 0, function* () {
    // Write code here that turns the phrase above into concrete actions
    let helyszinName = 'helyszin ' + Date.now();
    yield app.helyszinDetailPage.rovidNevField.sendKeys(helyszinName);
    yield app.helyszinDetailPage.hivatalosNevField.sendKeys(helyszinName);
    yield app.helyszinDetailPage.iranyitoszamField.sendKeys('1111');
    yield app.helyszinDetailPage.kozteruletNevDropdown.sendKeys('Budafoki');
    yield app.helyszinDetailPage.hazszamField.sendKeys('5');
}));
cucumber_1.When('I click on mentés button', () => __awaiter(void 0, void 0, void 0, function* () {
    // Write code here that turns the phrase above into concrete actions
    yield app.form.mentesButton.click();
}));
cucumber_1.When('I click on bezárás button', () => __awaiter(void 0, void 0, void 0, function* () {
    // Write code here that turns the phrase above into concrete actions
    yield app.form.bezarasButton.click();
}));
cucumber_1.When('I create a program helyszín with missing mandatory data', () => __awaiter(void 0, void 0, void 0, function* () {
    // Write code here that turns the phrase above into concrete actions
    let helyszinName = 'helyszin ' + Date.now();
    yield app.helyszinPage.ujHelyszinButton.click();
    yield app.helyszinDetailPage.rovidNevField.sendKeys(helyszinName);
    yield app.helyszinDetailPage.hivatalosNevField.sendKeys(helyszinName);
    yield app.form.mentesButton.click();
}));
cucumber_1.When('I click on modosítások elvetése button', () => __awaiter(void 0, void 0, void 0, function* () {
    // Write code here that turns the phrase above into concrete actions
    yield app.form.modositasokElveteseButton.click();
}));
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaGVseXN6aW5DcmVhdGlvblN0ZXBzLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiLi4vLi4vc3RlcERlZmluaXRpb25zL2hlbHlzemluQ3JlYXRpb25TdGVwcy50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7OztBQUFBLHVDQUFvQztBQUNwQywyQ0FBcUM7QUFFckMsa0RBQTRDO0FBQzVDLHNEQUF1RDtBQUd2RCxJQUFJLEdBQUcsR0FBRyxJQUFJLFlBQUcsRUFBRSxDQUFDO0FBQ3BCLElBQUksS0FBSyxHQUFHLElBQUkseUJBQWMsRUFBRSxDQUFDO0FBR2pDLGVBQUksQ0FBQywyQ0FBMkMsRUFBRSxDQUFPLE1BQU0sRUFBRSxPQUFPLEVBQUMsRUFBRTtJQUN2RSxvRUFBb0U7SUFDcEUsTUFBTSxLQUFLLENBQUMsYUFBYSxDQUFDLFFBQVEsQ0FBQyxjQUFjLENBQUMsQ0FBQztJQUNuRCxNQUFNLEtBQUssQ0FBQyxhQUFhLENBQUMsUUFBUSxDQUFDLE1BQU0sQ0FBQyxDQUFDO0lBQzNDLE1BQU0sS0FBSyxDQUFDLFdBQVcsQ0FBQyxLQUFLLEVBQUUsQ0FBQztBQUNwQyxDQUFDLENBQUEsQ0FBQyxDQUFDO0FBR0gsZUFBSSxDQUFDLHNDQUFzQyxFQUFFLEdBQVEsRUFBRTtJQUNuRCxvRUFBb0U7SUFDcEUsTUFBTSxvQkFBTyxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsQ0FBQztJQUMxQixNQUFNLEdBQUcsQ0FBQyxJQUFJLENBQUMsY0FBYyxFQUFFLENBQUM7QUFDcEMsQ0FBQyxDQUFBLENBQUMsQ0FBQztBQUVILGVBQUksQ0FBQyxpREFBaUQsRUFBRSxHQUFRLEVBQUU7SUFDOUQsb0VBQW9FO0lBQ3BFLE1BQU0sR0FBRyxDQUFDLFlBQVksQ0FBQyxnQkFBZ0IsQ0FBQyxLQUFLLEVBQUUsQ0FBQztBQUNwRCxDQUFDLENBQUEsQ0FBQyxDQUFDO0FBRUgsZUFBSSxDQUFDLGlGQUFpRixFQUFFLEdBQVEsRUFBRTtJQUM5RixvRUFBb0U7SUFDcEUsSUFBSSxZQUFZLEdBQUcsV0FBVyxHQUFHLElBQUksQ0FBQyxHQUFHLEVBQUUsQ0FBQztJQUM1QyxNQUFNLEdBQUcsQ0FBQyxrQkFBa0IsQ0FBQyxhQUFhLENBQUMsUUFBUSxDQUFDLFlBQVksQ0FBQyxDQUFDO0lBQ2xFLE1BQU0sR0FBRyxDQUFDLGtCQUFrQixDQUFDLGlCQUFpQixDQUFDLFFBQVEsQ0FBQyxZQUFZLENBQUMsQ0FBQztJQUN0RSxNQUFNLEdBQUcsQ0FBQyxrQkFBa0IsQ0FBQyxpQkFBaUIsQ0FBQyxRQUFRLENBQUMsTUFBTSxDQUFDLENBQUM7SUFDaEUsTUFBTSxHQUFHLENBQUMsa0JBQWtCLENBQUMscUJBQXFCLENBQUMsUUFBUSxDQUFDLFVBQVUsQ0FBQyxDQUFDO0lBQ3hFLE1BQU0sR0FBRyxDQUFDLGtCQUFrQixDQUFDLFlBQVksQ0FBQyxRQUFRLENBQUMsR0FBRyxDQUFDLENBQUM7QUFDNUQsQ0FBQyxDQUFBLENBQUMsQ0FBQztBQUVILGVBQUksQ0FBQywwQkFBMEIsRUFBRSxHQUFRLEVBQUU7SUFDdkMsb0VBQW9FO0lBQ3BFLE1BQU0sR0FBRyxDQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsS0FBSyxFQUFFLENBQUM7QUFDeEMsQ0FBQyxDQUFBLENBQUMsQ0FBQztBQUVILGVBQUksQ0FBQywyQkFBMkIsRUFBRSxHQUFRLEVBQUU7SUFDeEMsb0VBQW9FO0lBQ3BFLE1BQU0sR0FBRyxDQUFDLElBQUksQ0FBQyxhQUFhLENBQUMsS0FBSyxFQUFFLENBQUM7QUFDekMsQ0FBQyxDQUFBLENBQUMsQ0FBQztBQUVILGVBQUksQ0FBQyx5REFBeUQsRUFBRSxHQUFRLEVBQUU7SUFDdEUsb0VBQW9FO0lBQ3BFLElBQUksWUFBWSxHQUFHLFdBQVcsR0FBRyxJQUFJLENBQUMsR0FBRyxFQUFFLENBQUM7SUFDNUMsTUFBTSxHQUFHLENBQUMsWUFBWSxDQUFDLGdCQUFnQixDQUFDLEtBQUssRUFBRSxDQUFDO0lBQ2hELE1BQU0sR0FBRyxDQUFDLGtCQUFrQixDQUFDLGFBQWEsQ0FBQyxRQUFRLENBQUMsWUFBWSxDQUFDLENBQUM7SUFDbEUsTUFBTSxHQUFHLENBQUMsa0JBQWtCLENBQUMsaUJBQWlCLENBQUMsUUFBUSxDQUFDLFlBQVksQ0FBQyxDQUFDO0lBQ3RFLE1BQU0sR0FBRyxDQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsS0FBSyxFQUFFLENBQUM7QUFDeEMsQ0FBQyxDQUFBLENBQUMsQ0FBQztBQUVILGVBQUksQ0FBQyx3Q0FBd0MsRUFBRSxHQUFRLEVBQUU7SUFDckQsb0VBQW9FO0lBQ3BFLE1BQU0sR0FBRyxDQUFDLElBQUksQ0FBQyx5QkFBeUIsQ0FBQyxLQUFLLEVBQUUsQ0FBQztBQUNyRCxDQUFDLENBQUEsQ0FBQyxDQUFDIn0=