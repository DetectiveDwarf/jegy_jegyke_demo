"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const cucumber_1 = require("cucumber");
const app_po_1 = require("../pageObjects/app.po");
const form_po_1 = require("../pageObjects/form.po");
let app = new app_po_1.App();
cucumber_1.When('I click on Új jegytípus hozzáadása button', () => __awaiter(void 0, void 0, void 0, function* () {
    // Write code here that turns the phrase above into concrete actions
    yield app.jegytipusPage.ujJegytipusButton.click();
}));
cucumber_1.When('I make a new jegytípus and check the newly created jegytípus in the list page', () => __awaiter(void 0, void 0, void 0, function* () {
    // Write code here that turns the phrase above into concrete actions
    let jegytipusName = 'jegytipus ' + Date.now();
    yield app.menu.goToJegytipusok();
    yield app.jegytipusPage.ujJegytipusButton.click();
    yield app.jegytipusDetailPage.megnevezesField.sendKeys(jegytipusName);
    yield form_po_1.Form.selectDropdownItem(app.jegytipusDetailPage.korcsoportDropdown, 'Felnőtt (25-63)');
    yield form_po_1.Form.selectDropdownItem(app.jegytipusDetailPage.jegytipusKategoriaDropdown, 'Egyéni');
    yield app.jegytipusDetailPage.mentesButton.click();
    yield app.jegytipusDetailPage.bezarasButton.click();
    yield app.jegytipusPage.searchField.sendKeys(jegytipusName);
}));
cucumber_1.When('I make a new family jegytipus with discount and I check the newly created family jegytípus in the list page', () => __awaiter(void 0, void 0, void 0, function* () {
    // Write code here that turns the phrase above into concrete actions
    let jegytipusName = 'jegytipus ' + Date.now();
    yield app.menu.goToJegytipusok();
    yield app.jegytipusPage.ujJegytipusButton.click();
    yield app.jegytipusDetailPage.megnevezesField.sendKeys(jegytipusName);
    yield form_po_1.Form.selectDropdownItem(app.jegytipusDetailPage.jegyArTipusaDropdown, 'Kedvezményes');
    yield app.jegytipusDetailPage.ujkedvezmenyButton.click();
    yield form_po_1.Form.selectDropdownItem(app.jegytipusDetailPage.kedvezmenyDropdown, 'Családi kedvezmény');
    yield app.jegytipusDetailPage.hozzaadasButton.click();
    yield form_po_1.Form.selectDropdownItem(app.jegytipusDetailPage.korcsoportDropdown, 'Vegyes (0-99)');
    yield form_po_1.Form.selectDropdownItem(app.jegytipusDetailPage.jegytipusKategoriaDropdown, 'Családi');
    yield form_po_1.Form.selectRadioButton(app.jegytipusDetailPage.szemelyekSzamaRadioButton, 'Csoportos');
    yield app.jegytipusDetailPage.szemelyekSzamaField.sendKeys(5);
    yield app.jegytipusDetailPage.mentesButton.click();
    yield app.jegytipusDetailPage.bezarasButton.click();
    yield app.jegytipusPage.searchField.sendKeys(jegytipusName);
}));
cucumber_1.When('I create a new jegytipus than edit it', () => __awaiter(void 0, void 0, void 0, function* () {
    // Write code here that turns the phrase above into concrete actions
    let jegytipusName = 'jegytipus ' + Date.now();
    yield app.menu.goToJegytipusok();
    yield app.jegytipusPage.ujJegytipusButton.click();
    yield app.jegytipusDetailPage.megnevezesField.sendKeys(jegytipusName);
    yield app.jegytipusDetailPage.rovidKodField.sendKeys('AUTST');
    yield app.jegytipusDetailPage.rovidLeirasField.sendKeys('test leírás automata tesztből');
    yield form_po_1.Form.selectDropdownItem(app.jegytipusDetailPage.jegyArTipusaDropdown, 'Kedvezményes');
    yield app.jegytipusDetailPage.ujkedvezmenyButton.click();
    yield form_po_1.Form.selectDropdownItem(app.jegytipusDetailPage.kedvezmenyDropdown, 'Egyéb kártyakedvezmények');
    yield app.jegytipusDetailPage.hozzaadasButton.click();
    yield form_po_1.Form.selectDropdownItem(app.jegytipusDetailPage.korcsoportDropdown, 'Idős: (70-');
    yield form_po_1.Form.selectDropdownItem(app.jegytipusDetailPage.jegytipusKategoriaDropdown, 'Egyéni');
    yield app.jegytipusDetailPage.mentesButton.click();
    yield form_po_1.Form.selectDropdownItem(app.jegytipusDetailPage.jegyArTipusaDropdown, 'Teljes árú');
}));
cucumber_1.When('I create a jegytipus than inactivate it and I check the inactivated jegytipus among the jegytipusok with active state', () => __awaiter(void 0, void 0, void 0, function* () {
    // Write code here that turns the phrase above into concrete actions
    let jegytipusName = 'jegytipus ' + Date.now();
    yield app.menu.goToJegytipusok();
    yield app.jegytipusPage.ujJegytipusButton.click();
    yield app.jegytipusDetailPage.megnevezesField.sendKeys(jegytipusName);
    yield form_po_1.Form.selectDropdownItem(app.jegytipusDetailPage.korcsoportDropdown, 'Felnőtt (25-63)');
    yield form_po_1.Form.selectDropdownItem(app.jegytipusDetailPage.jegytipusKategoriaDropdown, 'Egyéni');
    yield app.jegytipusDetailPage.mentesButton.click();
    yield app.jegytipusDetailPage.inactivateButton.click();
    yield app.jegytipusDetailPage.confirmButton.click();
    yield app.jegytipusPage.searchField.sendKeys(jegytipusName);
}));
cucumber_1.When('I check the inactivated jegytipus among the jegytipusok with inactive state', () => __awaiter(void 0, void 0, void 0, function* () {
    // Write code here that turns the phrase above into concrete actions
    yield app.jegytipusPage.inactiveFilterCheckbox.click();
    yield app.jegytipusPage.activeFilterCheckbox.click();
}));
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiamVneXRpcHVzQ3JlYXRpb25TdGVwcy5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIi4uLy4uL3N0ZXBEZWZpbml0aW9ucy9qZWd5dGlwdXNDcmVhdGlvblN0ZXBzLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7O0FBQUEsdUNBQThCO0FBQzlCLGtEQUEwQztBQUMxQyxvREFBNEM7QUFHNUMsSUFBSSxHQUFHLEdBQUcsSUFBSSxZQUFHLEVBQUUsQ0FBQztBQUNwQixlQUFJLENBQUMsMkNBQTJDLEVBQUUsR0FBUSxFQUFFO0lBQ3hELG9FQUFvRTtJQUNwRSxNQUFNLEdBQUcsQ0FBQyxhQUFhLENBQUMsaUJBQWlCLENBQUMsS0FBSyxFQUFFLENBQUM7QUFDdEQsQ0FBQyxDQUFBLENBQUMsQ0FBQztBQUVILGVBQUksQ0FBQywrRUFBK0UsRUFBRSxHQUFRLEVBQUU7SUFDNUYsb0VBQW9FO0lBQ3BFLElBQUksYUFBYSxHQUFHLFlBQVksR0FBRyxJQUFJLENBQUMsR0FBRyxFQUFFLENBQUM7SUFDOUMsTUFBTSxHQUFHLENBQUMsSUFBSSxDQUFDLGVBQWUsRUFBRSxDQUFDO0lBQ2pDLE1BQU0sR0FBRyxDQUFDLGFBQWEsQ0FBQyxpQkFBaUIsQ0FBQyxLQUFLLEVBQUUsQ0FBQztJQUNsRCxNQUFNLEdBQUcsQ0FBQyxtQkFBbUIsQ0FBQyxlQUFlLENBQUMsUUFBUSxDQUFDLGFBQWEsQ0FBQyxDQUFDO0lBQ3RFLE1BQU0sY0FBSSxDQUFDLGtCQUFrQixDQUFDLEdBQUcsQ0FBQyxtQkFBbUIsQ0FBQyxrQkFBa0IsRUFBRSxpQkFBaUIsQ0FBQyxDQUFDO0lBQzdGLE1BQU0sY0FBSSxDQUFDLGtCQUFrQixDQUFDLEdBQUcsQ0FBQyxtQkFBbUIsQ0FBQywwQkFBMEIsRUFBRSxRQUFRLENBQUMsQ0FBQztJQUM1RixNQUFNLEdBQUcsQ0FBQyxtQkFBbUIsQ0FBQyxZQUFZLENBQUMsS0FBSyxFQUFFLENBQUM7SUFDbkQsTUFBTSxHQUFHLENBQUMsbUJBQW1CLENBQUMsYUFBYSxDQUFDLEtBQUssRUFBRSxDQUFDO0lBQ3BELE1BQU0sR0FBRyxDQUFDLGFBQWEsQ0FBQyxXQUFXLENBQUMsUUFBUSxDQUFDLGFBQWEsQ0FBQyxDQUFDO0FBQ2hFLENBQUMsQ0FBQSxDQUFDLENBQUM7QUFFSCxlQUFJLENBQUMsNkdBQTZHLEVBQUUsR0FBUSxFQUFFO0lBQzFILG9FQUFvRTtJQUNwRSxJQUFJLGFBQWEsR0FBRyxZQUFZLEdBQUcsSUFBSSxDQUFDLEdBQUcsRUFBRSxDQUFDO0lBQzlDLE1BQU0sR0FBRyxDQUFDLElBQUksQ0FBQyxlQUFlLEVBQUUsQ0FBQztJQUNqQyxNQUFNLEdBQUcsQ0FBQyxhQUFhLENBQUMsaUJBQWlCLENBQUMsS0FBSyxFQUFFLENBQUM7SUFDbEQsTUFBTSxHQUFHLENBQUMsbUJBQW1CLENBQUMsZUFBZSxDQUFDLFFBQVEsQ0FBQyxhQUFhLENBQUMsQ0FBQztJQUN0RSxNQUFNLGNBQUksQ0FBQyxrQkFBa0IsQ0FBQyxHQUFHLENBQUMsbUJBQW1CLENBQUMsb0JBQW9CLEVBQUUsY0FBYyxDQUFDLENBQUM7SUFDNUYsTUFBTSxHQUFHLENBQUMsbUJBQW1CLENBQUMsa0JBQWtCLENBQUMsS0FBSyxFQUFFLENBQUM7SUFDekQsTUFBTSxjQUFJLENBQUMsa0JBQWtCLENBQUMsR0FBRyxDQUFDLG1CQUFtQixDQUFDLGtCQUFrQixFQUFFLG9CQUFvQixDQUFDLENBQUM7SUFDaEcsTUFBTSxHQUFHLENBQUMsbUJBQW1CLENBQUMsZUFBZSxDQUFDLEtBQUssRUFBRSxDQUFDO0lBQ3RELE1BQU0sY0FBSSxDQUFDLGtCQUFrQixDQUFDLEdBQUcsQ0FBQyxtQkFBbUIsQ0FBQyxrQkFBa0IsRUFBRSxlQUFlLENBQUMsQ0FBQztJQUMzRixNQUFNLGNBQUksQ0FBQyxrQkFBa0IsQ0FBQyxHQUFHLENBQUMsbUJBQW1CLENBQUMsMEJBQTBCLEVBQUUsU0FBUyxDQUFDLENBQUM7SUFDN0YsTUFBTSxjQUFJLENBQUMsaUJBQWlCLENBQUMsR0FBRyxDQUFDLG1CQUFtQixDQUFDLHlCQUF5QixFQUFFLFdBQVcsQ0FBQyxDQUFDO0lBQzdGLE1BQU0sR0FBRyxDQUFDLG1CQUFtQixDQUFDLG1CQUFtQixDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUMsQ0FBQztJQUM5RCxNQUFNLEdBQUcsQ0FBQyxtQkFBbUIsQ0FBQyxZQUFZLENBQUMsS0FBSyxFQUFFLENBQUM7SUFDbkQsTUFBTSxHQUFHLENBQUMsbUJBQW1CLENBQUMsYUFBYSxDQUFDLEtBQUssRUFBRSxDQUFDO0lBQ3BELE1BQU0sR0FBRyxDQUFDLGFBQWEsQ0FBQyxXQUFXLENBQUMsUUFBUSxDQUFDLGFBQWEsQ0FBQyxDQUFDO0FBQ2hFLENBQUMsQ0FBQSxDQUFDLENBQUM7QUFFSCxlQUFJLENBQUMsdUNBQXVDLEVBQUUsR0FBUSxFQUFFO0lBQ3BELG9FQUFvRTtJQUNwRSxJQUFJLGFBQWEsR0FBRyxZQUFZLEdBQUcsSUFBSSxDQUFDLEdBQUcsRUFBRSxDQUFDO0lBQzlDLE1BQU0sR0FBRyxDQUFDLElBQUksQ0FBQyxlQUFlLEVBQUUsQ0FBQztJQUNqQyxNQUFNLEdBQUcsQ0FBQyxhQUFhLENBQUMsaUJBQWlCLENBQUMsS0FBSyxFQUFFLENBQUM7SUFDbEQsTUFBTSxHQUFHLENBQUMsbUJBQW1CLENBQUMsZUFBZSxDQUFDLFFBQVEsQ0FBQyxhQUFhLENBQUMsQ0FBQztJQUN0RSxNQUFNLEdBQUcsQ0FBQyxtQkFBbUIsQ0FBQyxhQUFhLENBQUMsUUFBUSxDQUFDLE9BQU8sQ0FBQyxDQUFDO0lBQzlELE1BQU0sR0FBRyxDQUFDLG1CQUFtQixDQUFDLGdCQUFnQixDQUFDLFFBQVEsQ0FBQywrQkFBK0IsQ0FBQyxDQUFDO0lBQ3pGLE1BQU0sY0FBSSxDQUFDLGtCQUFrQixDQUFDLEdBQUcsQ0FBQyxtQkFBbUIsQ0FBQyxvQkFBb0IsRUFBRSxjQUFjLENBQUMsQ0FBQztJQUM1RixNQUFNLEdBQUcsQ0FBQyxtQkFBbUIsQ0FBQyxrQkFBa0IsQ0FBQyxLQUFLLEVBQUUsQ0FBQztJQUN6RCxNQUFNLGNBQUksQ0FBQyxrQkFBa0IsQ0FBQyxHQUFHLENBQUMsbUJBQW1CLENBQUMsa0JBQWtCLEVBQUUsMEJBQTBCLENBQUMsQ0FBQztJQUN0RyxNQUFNLEdBQUcsQ0FBQyxtQkFBbUIsQ0FBQyxlQUFlLENBQUMsS0FBSyxFQUFFLENBQUM7SUFDdEQsTUFBTSxjQUFJLENBQUMsa0JBQWtCLENBQUMsR0FBRyxDQUFDLG1CQUFtQixDQUFDLGtCQUFrQixFQUFFLFlBQVksQ0FBQyxDQUFDO0lBQ3hGLE1BQU0sY0FBSSxDQUFDLGtCQUFrQixDQUFDLEdBQUcsQ0FBQyxtQkFBbUIsQ0FBQywwQkFBMEIsRUFBRSxRQUFRLENBQUMsQ0FBQztJQUM1RixNQUFNLEdBQUcsQ0FBQyxtQkFBbUIsQ0FBQyxZQUFZLENBQUMsS0FBSyxFQUFFLENBQUM7SUFDbkQsTUFBTSxjQUFJLENBQUMsa0JBQWtCLENBQUMsR0FBRyxDQUFDLG1CQUFtQixDQUFDLG9CQUFvQixFQUFFLFlBQVksQ0FBQyxDQUFDO0FBQzlGLENBQUMsQ0FBQSxDQUFDLENBQUM7QUFFSCxlQUFJLENBQUMsdUhBQXVILEVBQUUsR0FBUSxFQUFFO0lBQ3BJLG9FQUFvRTtJQUNwRSxJQUFJLGFBQWEsR0FBRyxZQUFZLEdBQUcsSUFBSSxDQUFDLEdBQUcsRUFBRSxDQUFDO0lBQzlDLE1BQU0sR0FBRyxDQUFDLElBQUksQ0FBQyxlQUFlLEVBQUUsQ0FBQztJQUNqQyxNQUFNLEdBQUcsQ0FBQyxhQUFhLENBQUMsaUJBQWlCLENBQUMsS0FBSyxFQUFFLENBQUM7SUFDbEQsTUFBTSxHQUFHLENBQUMsbUJBQW1CLENBQUMsZUFBZSxDQUFDLFFBQVEsQ0FBQyxhQUFhLENBQUMsQ0FBQztJQUN0RSxNQUFNLGNBQUksQ0FBQyxrQkFBa0IsQ0FBQyxHQUFHLENBQUMsbUJBQW1CLENBQUMsa0JBQWtCLEVBQUUsaUJBQWlCLENBQUMsQ0FBQztJQUM3RixNQUFNLGNBQUksQ0FBQyxrQkFBa0IsQ0FBQyxHQUFHLENBQUMsbUJBQW1CLENBQUMsMEJBQTBCLEVBQUUsUUFBUSxDQUFDLENBQUM7SUFDNUYsTUFBTSxHQUFHLENBQUMsbUJBQW1CLENBQUMsWUFBWSxDQUFDLEtBQUssRUFBRSxDQUFDO0lBQ25ELE1BQU0sR0FBRyxDQUFDLG1CQUFtQixDQUFDLGdCQUFnQixDQUFDLEtBQUssRUFBRSxDQUFDO0lBQ3ZELE1BQU0sR0FBRyxDQUFDLG1CQUFtQixDQUFDLGFBQWEsQ0FBQyxLQUFLLEVBQUUsQ0FBQztJQUNwRCxNQUFNLEdBQUcsQ0FBQyxhQUFhLENBQUMsV0FBVyxDQUFDLFFBQVEsQ0FBQyxhQUFhLENBQUMsQ0FBQztBQUNoRSxDQUFDLENBQUEsQ0FBQyxDQUFDO0FBRUgsZUFBSSxDQUFDLDZFQUE2RSxFQUFFLEdBQVEsRUFBRTtJQUMxRixvRUFBb0U7SUFDcEUsTUFBTSxHQUFHLENBQUMsYUFBYSxDQUFDLHNCQUFzQixDQUFDLEtBQUssRUFBRSxDQUFDO0lBQ3ZELE1BQU0sR0FBRyxDQUFDLGFBQWEsQ0FBQyxvQkFBb0IsQ0FBQyxLQUFLLEVBQUUsQ0FBQztBQUN6RCxDQUFDLENBQUEsQ0FBQyxDQUFDIn0=