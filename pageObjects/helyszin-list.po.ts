import {by, element, ElementFinder} from 'protractor';

export class HelyszinPage {
    ujHelyszinButton:ElementFinder;
    tableRows = element.all(by.css('.divTableRow'));

    constructor()
    {
        this.ujHelyszinButton = element(by.buttonText('Új programhelyszín hozzáadása'));
        this.tableRows = element.all(by.css('.divTableRow'));
    }

}
