import {by, element, ElementFinder} from 'protractor';
import { Form } from './form.po';

export class HelyszinDetailPage {
    rovidNevField:ElementFinder;
    hivatalosNevField:ElementFinder;
    iranyitoszamField:ElementFinder;
    telepulesDropdown:ElementFinder;
    kozteruletNevDropdown:ElementFinder;
    kozteruletJellegDropdown:ElementFinder;
    hazszamField:ElementFinder;
    cimKiegeszitesField:ElementFinder;
    megjegyzesField:ElementFinder;

    constructor()
    {
        this.rovidNevField = Form.getInput('rovidNev');
        this.hivatalosNevField = Form.getInput('hosszuNev');
        this.iranyitoszamField = Form.getInputMask('iranyitoszam');
        this.telepulesDropdown = Form.getAutocomplete('telepules');
        this.kozteruletNevDropdown = Form.getAutocomplete('selectedAddress');
        this.kozteruletJellegDropdown = Form.getAutocomplete('kozteruletTipus');
        this.hazszamField = Form.getInput('hazszam');
        this.cimKiegeszitesField = Form.getInput('cimkiegeszites');
        this.megjegyzesField = Form.getInput('leiras');
    }


/*
    async createHelyszin(helyszinName: string) {
        const form = new Form();
        await this.rovidNevField.sendKeys(helyszinName);
        await this.hivatalosNevField.sendKeys(helyszinName);
        await this.iranyitoszamField.sendKeys('1111');
        await this.kozteruletNevDropdown.sendKeys('Budafoki');
        await this.hazszamField.sendKeys('5');
        await form.mentesButton.click();
    }

 */
}
