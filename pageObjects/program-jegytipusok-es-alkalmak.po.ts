import { by, element } from 'protractor';
import { Form } from './form.po';

export class JegytipusokEsAlkalmakPage {
    ujAlkalomRogziteseButton = element(by.partialButtonText('Új alkalmak rögzítése'));
    alkalomMegnevezesField = Form.getInput('megnevezes');
    hetiIsmetlodesRadioButton = Form.getRadioButton('alkalmakJellege', 'Heti ismétlődés');
    konkretAlkalmakRadioButton = Form.getRadioButton('alkalmakJellege', 'Konkrét alkalmak');
    nincsLimitRadiobutton = Form.getRadioButton('ertekesitesiKorlat', 'Nincs');
    alkalomLimitRadiobutton = Form.getRadioButton('ertekesitesiKorlat', 'Alkalmakra (fő)');
    jegytipusLimitRadiobutton = Form.getRadioButton('ertekesitesiKorlat', 'Jegytípusra (db)');
    ervenyessegKezdeteField = Form.getInput('ervenyessegKezdete');
    ervenyessegVegeField = Form.getInput('ervenyessegVege');

    hetfoCheckbox = Form.getCheckboxByFormControlName('hetfo');
    keddCheckbox = Form.getCheckboxByFormControlName('kedd');
    szerdaCheckbox = Form.getCheckboxByFormControlName('szerda');
    csutortokCheckbox = Form.getCheckboxByFormControlName('csutortok');
    pentekCheckbox = Form.getCheckboxByFormControlName('pentek');
    szombatCheckbox = Form.getCheckboxByFormControlName('szombat');
    vasarnapCheckbox = Form.getCheckboxByFormControlName('vasarnap');

    jegytipusCheckbox = Form.getCheckboxByFormControlName('active');

    arField = Form.getInput('ar');
}
