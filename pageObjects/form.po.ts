import { by, element, ElementFinder } from 'protractor';

export class Form {
    // Form actions
    modositasokElveteseButton = element(by.partialButtonText('Módosítások elvetése'));
    mentesButton = element(by.partialButtonText('Mentés'));
    bezarasButton = element(by.partialButtonText('Bezárás'));

    public static getInput(formControlName: string): ElementFinder {
        return element(by.css('input[formcontrolname=' + formControlName + ']'));
    }

    public static getTextarea(formControlName: string) {
        return element(by.css('textarea[formcontrolname=' + formControlName + ']'));
    }

    public static getInputMask(formControlName: string): ElementFinder {
        return element(by.css('p-inputmask[formcontrolname=' + formControlName + '] input'));
    }

    public static getAutocomplete(formControlName: string): ElementFinder {
        return element(by.css('p-autocomplete[formcontrolname=' + formControlName + '] input'));
    }

    public static getDropDown(formControlName: string): ElementFinder {
        return element(by.css('p-dropdown[formcontrolname=' + formControlName + ']'));
    }

    private static getDropDownItem(itemName: string): ElementFinder {
        return element(by.cssContainingText('li span', itemName));
    }

    public static async selectDropdownItem(formControlName: string, itemName: string) {
        await this.getDropDown(formControlName).click();
        await this.getDropDownItem(itemName).click();
    }

    public static getRadioButton(formControlName: string, label: string): ElementFinder {
        return element(by.cssContainingText('p-radiobutton[formcontrolname=' + formControlName + '] label', label));
    }

    public static async selectRadioButton(formControlName: string, label: string) {
        await this.getRadioButton(formControlName, label).click();
    }

    public static getCheckboxByName(name: string, label: string): ElementFinder {
        return element(by.cssContainingText('p-checkbox[name=' + name + '] label', label));
    }

    public static getCheckboxByFormControlName(formcontrolname: string): ElementFinder {
        return element(by.css('p-checkbox[formcontrolname=' + formcontrolname + ']'));
    }

    public static getDateInput(formControlName: string): ElementFinder {
        return element(by.css('app-custom-datepicker[formcontrolname=' + formControlName + '] input'));
    }
}
