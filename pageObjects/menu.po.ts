import {browser, by, element, ElementFinder} from 'protractor';


export class MenuPage
{
    /**
     * Felhasználói menü ikon
     */
    userMenuButton:ElementFinder;

    /**
     * Kilépés gomb
     */

    kilepesUserMenuButton:ElementFinder;

 beallitasokMenu:ElementFinder;
 jegytipusMenu:ElementFinder;
 helyszinMenu:ElementFinder;
 programMenu:ElementFinder;

    constructor()
    {
        this.userMenuButton = element(by.css('button[type="button"].user-icon'));
        this.kilepesUserMenuButton = element(by.cssContainingText('span.ui-menuitem-text', 'Kilépés'));
        this.beallitasokMenu = element(by.cssContainingText('span[class="ui-menuitem-text"]', 'Beállítások'));
        this.jegytipusMenu = element(by.cssContainingText('span[class="ui-menuitem-text"]', 'Jegytípusok'));
        this.helyszinMenu = element(by.cssContainingText('span[class="ui-menuitem-text"]', 'Programhelyszínek'));
        this.programMenu = element(by.cssContainingText('span[class="ui-menuitem-text"]', 'Programok'));
    }

    async goToJegytipusok() {
        await this.beallitasokMenu.click();
        await this.jegytipusMenu.click();
    }

    async logout() {
        await this.userMenuButton.click();
        await this.kilepesUserMenuButton.click();
    }

    async goToHelyszinek() {
        await this.beallitasokMenu.click();
        await this.helyszinMenu.click();
    }
}