import { browser, by, element } from 'protractor';
import { IWebDriverOptionsCookie } from 'selenium-webdriver';

export class KauLoginPage {
    usernameField = element(by.css('input[name="felhasznaloNev"]'));
    passwordField = element(by.css('input[name="jelszo"]'));
    loginButton = element(by.css('button[type="submit"]'));
    welcomeText = element(by.tagName('h1'));

    // Emailes bejelentkezés alatti KAÜS login gomb
    kauButton = element(by.cssContainingText('span.ui-button-text', 'Belépés ügyfélkapu azonosítóval'));

    // Ügyfélkapu gomb a KAÜS bejelentkezés felületen
    ugyfelkapuButton = element(by.id('urn:eksz.gov.hu:1.0:azonositas:kau:1:uk:uidpwd'));

    // TODO A szerepkör érkezzen paraméterként és az alapján válassza ki a felhasználónevet és jelszót
    async kauLogin() {
        let cookie: IWebDriverOptionsCookie;
        await this.usernameField.sendKeys('ibolya.schoffer');
        await this.passwordField.sendKeys('jelszo12AA');
        await this.loginButton.click();

        // Manuálisan kell állítani az időzítést mert nem az Angularon belül vagyunk (és a háttérben sok átirányítás történik)
        // Előfordulhat, hogy növelni kell ezt az értéket ha a KAÜ valamiért lassan tölt be
        await browser.sleep(5000);
        // A KAÜ átirányít a jegyke-qa.sonrisa.hu -ra, kimásoljuk a cookiet és átírjuk a domaint
        /* await browser
           .manage()
           .getCookies()
           .then((cookies) => {
             cookie = cookies[0];
             cookie.domain = 'localhost';
           });
         await browser.sleep(5000);*/
        // Visszakapcsoljuk az Angular eseményekre a várakozást
        await browser.waitForAngularEnabled(true);
        // Visszalépünk az alkalmazásba
        //await browser.get('https://jegyke-qa.sonrisa.hu/');
        // Beállítjuk a cookiet
        //await browser.manage().addCookie(cookie);
        // Átnavigálunk a kezdőoldalra
        await browser.get('https://jegyke-qa.sonrisa.hu/');
    }
}
