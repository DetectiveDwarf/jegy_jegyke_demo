import { by, element } from 'protractor';

export class ShellPage {
    welcomeText = element(by.css('app-root h1'));
    toastMessage = element(by.css('.ui-toast-message'));

    getParagraphText() {
        return this.welcomeText.getText();
    }
}
