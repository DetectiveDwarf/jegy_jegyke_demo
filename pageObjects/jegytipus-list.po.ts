import { by, element } from 'protractor';
import { Form } from './form.po';

export class JegytipusListPage {
    ujJegytipusButton = element(by.buttonText('Új jegytípus hozzáadása'));
    searchField = element(by.css('.kereses-block input'));
    tableRows = element.all(by.css('.divTableRow'));
    inactiveFilterCheckbox = Form.getCheckboxByName('statuszSzuro', 'Inaktív');
    activeFilterCheckbox = Form.getCheckboxByName('statuszSzuro', 'Aktív');
}
