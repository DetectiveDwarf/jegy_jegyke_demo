import {by, element, ElementFinder} from "protractor";

export class kaulogin
{
    jegykeLoginButton:ElementFinder;
    kauCustomerPortalButton:ElementFinder;
    kauAccountField:ElementFinder;
    kauAccountPasswordField:ElementFinder;
    kauCustomerPortalLoginButton:ElementFinder;


    constructor()
    {
        this.jegykeLoginButton = element(by.cssContainingText('span.ui-button-text', 'Belépés ügyfélkapu azonosítóval'));
        this.kauCustomerPortalButton = element(by.className("btn"));
        this.kauAccountField = element(by.name("felhasznaloNev"));
        this.kauAccountPasswordField = element(by.name("jelszo"));
        this.kauCustomerPortalLoginButton = element(by.buttonText('bejelentkezés'));
    }
}