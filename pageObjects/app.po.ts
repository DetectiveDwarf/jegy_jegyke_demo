import { HelyszinDetailPage } from './helyszin-detail.po';
import { HelyszinPage } from './helyszin-list.po';
import { JegytipusDetailPage } from './jegytipus-detail.po';
import { JegytipusListPage } from './jegytipus-list.po';
import { KauLoginPage } from './kau-login.po';
import { EmailLoginPage } from './login.po';
import { MenuPage } from './menu.po';
import { ShellPage } from './shell.po';
import { Form } from './form.po';
import { ProgramPage } from './program.po';
import { ProgramListPage } from './program-list.po';

export class App {
    menu = new MenuPage();

    shell = new ShellPage();

    form = new Form();

    helyszinDetailPage = new HelyszinDetailPage();
    helyszinPage = new HelyszinPage();

    jegytipusDetailPage = new JegytipusDetailPage();
    jegytipusPage = new JegytipusListPage();

    program = new ProgramPage();
    programListPage = new ProgramListPage();

    kauLoginPage = new KauLoginPage();
    emailLoginPage = new EmailLoginPage();
}
