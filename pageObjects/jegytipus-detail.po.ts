import { by, element } from 'protractor';
import { Form } from './form.po';
import { App } from './app.po';

export class JegytipusDetailPage {
    megnevezesField = Form.getInput('megnevezes');
    rovidKodField = Form.getInput('rovidKod');
    rovidLeirasField = Form.getInput('leiras');

    // Személyek száma
    szemelyekSzamaRadioButton = 'szemelyekSzama';
    szemelyekSzamaField = Form.getInput('csoportosSzemelyekNum');

    // Belépések száma
    belepesekSzamaRadioButton = 'belepesekSzama';
    belepesekSzamaField = Form.getInput('korlatozottBelepesekSzamaNum');

    // Felhasználható
    felhasznalhatoRadioButton = 'felhasznalhato';
    felhasznalhatoField = Form.getInput('felhasznalhatoNum');

    jegyArTipusaDropdown = 'arkepzes';
    korcsoportDropdown = 'korcsoport';
    jegytipusKategoriaDropdown = 'jegyrendszerKategoria';
    ujkedvezmenyButton = element(by.buttonText('Új kedvezmény rögzítése'));
    kedvezmenyDropdown = 'kedvezmeny';
    hozzaadasButton = element(by.buttonText('Hozzáadás'));
    kedvezmenyTorlesButton = element(by.css('.pi-times'));

    // Activity functions
    activateButton = element(by.partialButtonText('Jegytípus aktiválása'));
    inactivateButton = element(by.partialButtonText('Jegytípus inaktiválása'));
    confirmButton = element(by.partialButtonText('Megerősítés'));
    closeButton = element(by.partialButtonText('Bezárás'));

    // Form actions
    modositasokElveteseButton = element(by.buttonText('Módosítások elvetése'));
    mentesButton = element(by.buttonText('Mentés'));
    bezarasButton = element(by.buttonText('Bezárás'));

    async createJegytipus(jegytipusName: string) {
        const form = new Form();
        const app = new App();
        await this.megnevezesField.sendKeys(jegytipusName);
        await Form.selectDropdownItem(app.jegytipusDetailPage.korcsoportDropdown, 'Felnőtt (25-63)');
        await Form.selectDropdownItem(app.jegytipusDetailPage.jegytipusKategoriaDropdown, 'Egyéni');
        await form.mentesButton.click();
    }
}
