import { by, element } from 'protractor';
import { ProgramAlapadatokPage } from './program-alapadatok.po';
import { JegytipusokEsAlkalmakPage } from './program-jegytipusok-es-alkalmak.po';

export class ProgramPage {
    // Tabs
    alapadatokTab = element(by.buttonText('Alapadatok'));
    jegytipusokEsAlkalmakTab = element(by.buttonText('Jegytípusok és alkalmak'));
    kajutTab = element(by.buttonText('KAJÜT adatok'));

    // Activity buttons
    activateButton = element(by.partialButtonText('Program aktiválása'));
    inctivateButton = element(by.partialButtonText('Program inaktiválása'));
    deleteButton = element(by.partialButtonText('Program törlése'));

    alapadatok = new ProgramAlapadatokPage();
    jegytipusokEsAlkalmak = new JegytipusokEsAlkalmakPage();
}
