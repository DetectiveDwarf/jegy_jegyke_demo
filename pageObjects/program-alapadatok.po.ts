import { by, element } from 'protractor';
import { Form } from './form.po';

export class ProgramAlapadatokPage {
    normalProgramRadioButton = Form.getRadioButton('programTipus', 'Normál program');
    kombinaltProgramRadioButton = Form.getRadioButton('programTipus', 'Kombinált program');
    programNevField = Form.getInput('hivatalosNev');
    idegenNevField = Form.getInput('angolNev');
    gyakorisagDropdown = 'gyakorisag';
    programKezdeteDate = Form.getDateInput('kezdete');
    programVegeDate = Form.getDateInput('vege');
    jegyertekesitesKezdeteDate = Form.getDateInput('jegyertekesitesKezdete');
    jegyertekesitesVegeDate = Form.getDateInput('jegyertekesitesVege');
    jegyertekesitesMegegyezikRadioButton = Form.getRadioButton('jegyertekesitesElteroIdovel', 'A programmal megegyező');
    jegyertekesitesElterRadioButton = Form.getRadioButton('jegyertekesitesElteroIdovel', 'A programtól eltérő');
    fokategoriaDropdown = 'programFokategoria';
    alkategoriaDropdown = 'programAlkategoria';
    helyszinDropdown = 'helyszin';
    jegyRendszerAzonosizoField = Form.getInput('jegyRendszerProgramazonosito');
    leirasField = Form.getTextarea('leiras');
    weboldalField = Form.getTextarea('url');
    ujKapcsolodoProgramHozzaadasaButton = element(by.partialButtonText('Új program hozzáadása'));
    ujKapcsolodoProgramDropdown = 'hozzaadottProgram';
    hozzaAdasButton = element(by.partialButtonText('hozzáadás'));
    statusAktivText = element(by.css('.statusz.AKTIV'));
    statusInaktivText = element(by.css('.statusz.INAKTIV'));
}
