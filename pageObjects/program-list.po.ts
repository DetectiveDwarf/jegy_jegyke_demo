import { by, element } from 'protractor';
import { Form } from './form.po';

export class ProgramListPage {
    ujProgramButton = element(by.partialButtonText('Új program hozzáadása'));
    tableRows = element.all(by.css('.divTableRow'));
    searchField = element(by.css('.kereses-block input'));
    inactiveFilterCheckbox = Form.getCheckboxByName('statuszSzuro', 'Inaktív');
    activeFilterCheckbox = Form.getCheckboxByName('statuszSzuro', 'Aktív');
    toroltFilterCheckbox = Form.getCheckboxByName('statuszSzuro', 'Törölt');
}
