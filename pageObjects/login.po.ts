import {by, element, ElementFinder} from 'protractor';
import { Credentials } from '../models/credentials.dto';
import { UserType } from '../models/user-type.enum';

const TEST_USER_CREDENTIALS: Map<UserType, Credentials> = new Map([
    [UserType.NO_EMAIL, new Credentials('', 'Jelszo1234')],
    [UserType.NO_PASSWORD, new Credentials('test@temp.hu', '')],
    [UserType.WRONG_PASSWORD, new Credentials('test@temp.hu', 'wrong password')],
    [UserType.WRONG_EMAIL, new Credentials('wrong_email@temp.hu', 'user')],
    [UserType.JEGYERTEKESITO, new Credentials('test@temp.hu', 'user')],
    [UserType.JEGYERVENYESITO, new Credentials('test@temp.hu', 'user')],
    [UserType.SZOLGMENEDZSER, new Credentials('test@temp.hu', 'user')],
]);

export class EmailLoginPage {
    usernameField: ElementFinder;
    passwordField: ElementFinder;
    loginButton: ElementFinder;

    constructor()
    {
        this.usernameField = element(by.css('input[formControlName="email"]'));
        this.passwordField = element(by.css('input[formControlName="password"]'));
        this.loginButton = element(by.buttonText('Bejelentkezés'))
    }

    // TODO A szerepkör érkezzen paraméterként és az alapján válassza ki a felhasználónevet és jelszót

    async login(role: UserType) {
        let credentials = TEST_USER_CREDENTIALS.get(role);
        //await this.usernameField.sendKeys(credentials.email);
        //await this.passwordField.sendKeys(credentials.password);
        await this.loginButton.click();
    }
}
